# BUT-RT-BlackPN 2021


## Installation 

### sur Mac, avec Anaconda:

    conda install pypandoc
    conda install ruamel
    conda install ruamel.yaml
    pip install docx2python
    pip install Jinja2

Sans Anaconda, remplacer `conda` par `pip`.

## Utilisation

### Téléchargement des fiches depuis le google drive du BUT

Les sources sont les fichiers GoogleDocx (drive) téléchargés en local dans le répertoire google/ en partant du document de synthèse BUT-RT-S1-S6.xlsx en utilisant les liens de la première feuille et en utilisant l'API Google Drive. 

Le téléchargement suppose de configurer l'OAuth sur un compte google par le biais d'une application déclarée sur cloud.google, avec une cle d'authentification stockée dans `export/google` (à créer). 

Pour lancer le téléchargement :

    cd python 
    python export_fiches.py


### Extraction des informations

Les fichiers `.docx` (téléchargés dans `export/google`) sont analysés pour créer les documents `yaml` (dans un `REPERTOIRE_EXPORT` configurable, configuration à revoir), avec le script `export_doc_to_yaml`.

L'analyse s'appuie sur les fichiers `python/pn/ressources.yml` et `python/pn/saes.yml` qui traduisent le découpage en ressources/SAés et les noms actés par le GT-BUT (cf. tableur google BUT RT S1-S6).



    cd python
    python export_docx_to_yaml.py -o REPERTOIRE_EXPORT [nom du fichier docx]

Par exemple:

    python export_docx_to_yaml.py import/compilation-ressources.docx


### Génération du LaTeX

Les documents `yaml` décrivant les ressources et les SAés créés (dans un `REPERTOIRE_EXPORT`) sont retraités pour générer différents fichiers latex nécessaires à la compilation du PN en PDF

    cd python
    python export_yaml_to_latex.py -a

Les fichiers latex se basent sur des modèles fournis dans `python/templates`.

Le script suppose que l'arborescence (partiel) suivante :

    python
        |- codes python ...
    yaml
        |- ressources
        |   |- les yaml de ressources
        |- saes
            |- les yaml de saes
    export
        |- latex
            |- ressources
            |   |- les tex de ressources
            |- saes
            |   |- les tex de saes
            |- synthese
            |   |- les tex des tableaux de synthèse (matrices AC/comp)

### Génération de PDF

    cd export/latex
    pdflatex pn_formation
    pdflatex pn_formation

Le résultat est `pn_formation.pdf`.

### Génération du Html (à reprendre)

    cd python
    python export_yaml_to_html.py

## Organisation des fichiers

    html/export   fichiers html générés par export_yaml_to_html.py à partir des yaml
    python/import   fichiers docx à traiter
    python/export   fichier yaml générés par export_docx_to_yaml.py à partir des docx
    yaml/ressources versions éditées manuellement ??
    latex/ressources    fiches au format LaTeX générées par export_yaml_to_latex.py
