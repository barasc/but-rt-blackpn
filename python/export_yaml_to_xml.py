import argparse
import logging
import sys
import os
import lxml.etree as ET

from config import Config

__LOGGER = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description="Conversion des YAML en XML")
parser.add_argument(
    "-a", 
    "--all", 
    action="store_true", 
    help="exporte le yaml complet"
    )
parser.add_argument(
    "-r", 
    "--root", 
    default="../export",
    help="repertoire de base (racine) pour chercher les fichiers de données"
    )
parser.add_argument(
    "-p",
    "--parcours",
    type=str,
    help="génère une version pour un parcours donné"
    )

args = parser.parse_args()
Config.ROOT = args.root

# LIMIT_TO = ["SAÉ3.1"]
LIMIT_TO = [] #"SAÉ6.6"] #"R3.02"]

# logging.basicConfig(filename='export_latex.log.txt', level=logging.WARNING)

import rpn.semestre, rofficiel.officiel, rpn.activite

REPERTOIRE_RESSOURCES_DEFINITIVES = Config.ROOT + "/yaml/ressources"
REPERTOIRE_SAE_DEFINITIVES = Config.ROOT + "/yaml/saes"

REPERTOIRE_XML = Config.ROOT + "/xml"

# Chargement des ressources, des SAés et des exemples
pnofficiel = rofficiel.officiel.Officiel() # charge les données officielles
semestres = {"S{}".format(d) : None for d in range(1, 7)}
# semestres = {}
semestres["SC"] = None
print("***Etape 1*** Chargement des yaml")
for sem in semestres:
    print(f" > Semestre {sem}")
    semestres[sem] = rpn.semestre.SemestrePN(sem,
                                         REPERTOIRE_RESSOURCES_DEFINITIVES,
                                         REPERTOIRE_SAE_DEFINITIVES,
                                         pnofficiel)

# Chargement des saé et des exemples


## Bilan : acs, volume, coefficient, abbréviations

ref_xml = ET.Element("referentiel_formation")
for sem in semestres:
    ## Fichiers d'inclusion des semestres
    if sem != "SC":
        semestres[sem].to_xml(ref_xml)
chaine = ET.tostring(ref_xml, pretty_print=True) # ET.dump(donnees)
chaine = chaine.decode("utf8") #decode('unicode-escape')
entites = {"&#233;": "é",
           #"&#339;": "oeuvre "
           "&#232;": "è",
           "&#201;": "É",
           "&#224;": "à", #"À",
           "&#8230;": "...",
           "&#039;": "'",
           "’": "'",
           "&#234;": "ê",
           "&#231;": "ç",
           "&#238;": "î"
           }
for ent in entites:
    chaine = chaine.replace(ent, entites[ent])
# print(chaine)
fichier = REPERTOIRE_XML + "/" + f"referentiel_RT.xml"
with open(fichier, "w", encoding="utf8") as fid:
    fid.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    fid.write(chaine)
print(f"Export de {fichier}")

