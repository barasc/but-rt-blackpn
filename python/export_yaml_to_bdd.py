import argparse
import logging
import sys
import os
import sqlite3

from config import Config

__LOGGER = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description="Conversion des YAML en BDD sqlite")
parser.add_argument(
    "-r", 
    "--root", 
    default="../export",
    help="repertoire de base (racine) pour chercher les fichiers de données"
    )

args = parser.parse_args()
Config.ROOT = args.root


# LIMIT_TO = ["SAÉ3.1"]
LIMIT_TO = [] # "SAÉ4.22"] #"SAÉ3.03"]  # "SAÉ3.04"] #"SAÉ6.6"] #"R3.02"]

# logging.basicConfig(filename='export_latex.log.txt', level=logging.WARNING)

import rpn.semestre, rofficiel.officiel, rpn.activite, rbdd.rbdd

REPERTOIRE_RESSOURCES_DEFINITIVES = Config.ROOT + "/yaml/ressources"
REPERTOIRE_SAE_DEFINITIVES = Config.ROOT + "/yaml/saes"


# Création de la BDD
BDD = Config.ROOT + "/bdd/pn.sqlite"
print(f"Suppression de la BDD {BDD}")
# os.remove(BDD)

# Création du connecteur
connexion = sqlite3.connect(BDD)

if False:
    print(f"Création de la BDD {BDD}")
    tables = ["parcours", "competences", "composantes", "situations", "niveaux", "acs",
              "annees", "semestres", "activites", "coche", "coeff", "heures", "prerequis", "mobilise"]
    for table in tables:
        rbdd.rbbd.execute_fichier_requetes(connexion, f"creation_{table}.sql")

# Chargement des compétences
pnofficiel = rofficiel.officiel.Officiel() # charge les données officielles
pnofficiel.to_bdd(connexion)

# Chargement des ressources, des SAés et des exemples

semestres = {"S{}".format(d) : None for d in range(1, 7)}
# semestres = {}
semestres["SC"] = None
print("***Etape 1*** Chargement des yaml")
for sem in semestres:
    print(f" > Semestre {sem}")
    semestres[sem] = rpn.semestre.SemestrePN(sem,
                                         REPERTOIRE_RESSOURCES_DEFINITIVES,
                                         REPERTOIRE_SAE_DEFINITIVES,
                                         pnofficiel)
    semestres[sem].to_bdd(connexion)

connexion.close()
# Chargement des saé et des exemples

