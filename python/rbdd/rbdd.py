
import logging

LOGGER = logging.getLogger("rbdd")
REPERTOIRE_ADE_BDD = "rbdd"


def execute_fichier_requetes(connexion, fichier):
    """Exécute une série de requêtes données dans un fichier"""
    rs = open(f"{REPERTOIRE_ADE_BDD}/sql/{fichier}", "r", encoding="utf8").read()
    liste_requete = rs.split(";")
    liste_requete = [r for r in liste_requete if r.strip()]  # ne conserve que les requêtes
    try:
        c = connexion.cursor()
        for r in liste_requete:
            c.execute(r)  # exécute la requete
    except:
        LOGGER.warning("Erreur dans l'exécution des requêtes de création de tables")
    finally:
        c.close()
    connexion.commit()
