CREATE TABLE activites (
  code VARCHAR(20) PRIMARY KEY, -- codeRT
  id_semestre INTEGER,
  ordre INTEGER,
  nom VARCHAR(150),
  diminutif VARCHAR(20),
  -- motscles VARCHAR(150), => table
  contexte VARCHAR(1500), -- objectifs
  description VARCHAR(1500), -- contenu
--
  CONSTRAINT fk_activ_sem FOREIGN KEY (id_semestre) REFERENCES semestre
);

CREATE TABLE ressources (
    code VARCHAR(20),
    preconisation VARCHAR(1500),
    prolongrement VARCHAR(1500),
    ---
    CONSTRAINT he_act_ress FOREIGN KEY (code) REFERENCES activites,
    CONSTRAINT pk_ress PRIMARY KEY (code)
);


CREATE TABLE saes (
    code VARCHAR(20),
    livrable VARCHAR(1500),
    ---
    CONSTRAINT he_act_saes FOREIGN KEY (code) REFERENCES activites,
    CONSTRAINT pk_saes PRIMARY KEY (code)
);

CREATE TABLE ref_form (
    code_parcours VARCHAR(10),
    code_activite VARCHAR(20),
    --
    CONSTRAINT fk_ref_form_parc FOREIGN KEY (code_parcours) REFERENCES parcours,
    CONSTRAINT fk_ref_form_act FOREIGN KEY (code_activite) REFERENCES activites,
    CONSTRAINT pk_ref_form PRIMARY KEY (code_parcours, code_activite)
);