CREATE TABLE parcours (
    code VARCHAR(10) PRIMARY KEY,
    intitule VARCHAR(30) UNIQUE
);

INSERT INTO parcours (code, intitule)
VALUES ("Cyber", "Cybersécurité"),
       ("DevCloud", "Développement Système et Cloud"),
       ("IOM", "Internet des Objets et Mobilité"),
       ("ROM", "Réseaux Opérateurs et Multimédia"),
       ("PilPro", "Pilotage de Projets réseaux");