--- CREATION des années
CREATE TABLE annees (
    id INTEGER PRIMARY KEY
);

INSERT INTO annees (id) VALUES (1), (2), (3);