CREATE TABLE semestre (
    id INTEGER PRIMARY KEY,
    id_annee INTEGER,
    --
    CONSTRAINT fk_sem_annee FOREIGN KEY (id_annee) REFERENCES annees
);

INSERT INTO semestre (id, id_annee) VALUES (1, 1), (2, 1), (3, 2), (4, 2), (5, 3), (6, 3);