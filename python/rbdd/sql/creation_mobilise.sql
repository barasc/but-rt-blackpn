CREATE TABLE mobilise (
    code_sae VARCHAR(20),
    code_ressource VARCHAR(20),
    --
    CONSTRAINT fk_mob_sae FOREIGN KEY (code_sae) REFERENCES saes,
    CONSTRAINT fk_mob_res FOREIGN KEY (code_ressource) REFERENCES ressources,
    CONSTRAINT pk_mob PRIMARY KEY (code_sae, code_ressource)
);