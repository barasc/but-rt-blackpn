CREATE TABLE heures (
    code_activite VARCHAR(20),
    type VARCHAR(10), -- TD/TP/encadree/projet
    heure FLOAT,
    CONSTRAINT fk_heures_act FOREIGN KEY (code_activite) REFERENCES activites,
    CONSTRAINT pk_heures PRIMARY KEY (code_activite, type)
);