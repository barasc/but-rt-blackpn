-- Création des niveaux
CREATE TABLE niveaux (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    niveau INTEGER,
    intitule VARCHAR(150) UNIQUE,
    code_comp VARCHAR(10),
    --
    CONSTRAINT fk_niv_comp FOREIGN KEY (code_comp) REFERENCES competences,
    CONSTRAINT un_niv UNIQUE (niveau, code_comp)
);

