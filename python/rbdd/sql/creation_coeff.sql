CREATE TABLE coeff (
    code_comp VARCHAR(10),
    code_activite VARCHAR(20),
    coeff INTEGER,
    --
    CONSTRAINT fk_coeff_comp FOREIGN KEY (code_comp) REFERENCES competences,
    CONSTRAINT fk_coeff_activite FOREIGN KEY (code_activite) REFERENCES activites,
    CONSTRAINT pk_coeff PRIMARY KEY (code_comp, code_activite)
);