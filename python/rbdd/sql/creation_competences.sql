-- Création des compétences
CREATE TABLE competences (
    code VARCHAR(10) PRIMARY KEY, -- Cyber1
    diminutif VARCHAR(15) UNIQUE, -- Verbe
    nom VARCHAR(30) UNIQUE, -- Titre
    description VARCHAR(1000)
);

-- L'association à un parcours
CREATE TABLE ref_competences (
    code_parcours VARCHAR(10),
    code_comp VARCHAR(10),
    --
    CONSTRAINT fk_ref_comp_parcours FOREIGN KEY (code_parcours) REFERENCES parcours,
    CONSTRAINT fk_ref_comp_comp FOREIGN KEY (code_comp) REFERENCES competences,
    CONSTRAINT pk_ref_comp PRIMARY KEY (code_parcours, code_comp)
);