-- Création des composantes essentielles
CREATE TABLE composantes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    code VARCHAR(10) , -- CE1.01
    intitule VARCHAR(150) UNIQUE, -- La compo
    code_comp VARCHAR(10),
    --
    CONSTRAINT fk_compo_compe FOREIGN KEY (code_comp) REFERENCES competences
);

