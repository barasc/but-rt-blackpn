CREATE TABLE prerequis (
    code_ressource VARCHAR(20),
    code_prerequis VARCHAR(20),
    --
    CONSTRAINT fk_pr_res FOREIGN KEY (code_ressource) REFERENCES ressources,
    CONSTRAINT fk_pr_res FOREIGN KEY (code_prerequis) REFERENCES ressources,
    CONSTRAINT pk_pr PRIMARY KEY (code_ressource, code_prerequis)
);