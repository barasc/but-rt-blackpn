CREATE TABLE coche (
    id_ac INTEGER,
    code_activite VARCHAR(20),
    optionnel BOOLEAN DEFAULT FALSE,
    -- 
    CONSTRAINT fk_coche_ac FOREIGN KEY (id_ac) REFERENCES acs,
    CONSTRAINT fk_coche_activite FOREIGN KEY (code_activite) REFERENCES activites,
    CONSTRAINT pk_coche PRIMARY KEY (id_ac, code_activite)     
);