-- Création des apprentissages critiques
CREATE TABLE acs (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    code VARCHAR(20),
    intitule VARCHAR(150) UNIQUE,
    id_niveau INTEGER,
    --
    CONSTRAINT fk_ac_niveau FOREIGN KEY (id_niveau) REFERENCES niveaux
);

