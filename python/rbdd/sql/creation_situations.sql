-- Création des situations professionnelles
CREATE TABLE situations (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    intitule VARCHAR(150) UNIQUE, -- La situa
    code_comp VARCHAR(10),
    --
    CONSTRAINT fk_situ_compe FOREIGN KEY (code_comp) REFERENCES competences
);

