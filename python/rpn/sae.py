import logging

import rpn.latex
from config import Config
import rpn.activite
import modeles
import lxml.etree as ET

class SAE(rpn.activite.ActivitePedagogique):
    """Modélise une SAé (chapeau) en chargeant les données provenant du ``fichieryaml``
    et stocke les données ``officiel``les.
    """

    __LOGGER = logging.getLogger(__name__)

    def __init__(self, fichieryaml, officiel):
        super().__init__(fichieryaml, officiel)
        print(fichieryaml)
        self.nom = self.yaml["nom"]
        self.codeOReBUT = self.yaml["codeOReBUT"]
        self.acs = self.yaml["acs"]
        self.acs_optionnels = self.yaml["acs_optionnels"]
        self.ordre = self.yaml["ordre"]
        # self.competences = self.yaml["competences"]
        self.annee = self.yaml["annee"]
        # self.heures_encadrees = self.yaml["heures_encadrees"]
        self.parcours = self.yaml["parcours"]
        self.coeffs = self.yaml["coeffs"]

        self.heures = self.yaml["heures"]


        # L'adaptation locale
        self.type = self.yaml["type"]
        self.adaptation = self.yaml["type"]["adaptation"]

        self.exemples = [] # la liste des exemples
        self.realisations = []

    def charge_exemple(self, e):
        """Charge un exemple"""
        self.exemples.append(e)

    def charge_realisation(self, rea):
        """Charge une réalisation"""
        self.realisations.append(rea)


    def to_latex(self, modele="templates/modele_tableau_sae.tex",
                 modele_light="templates/modele_tableau_sae_light.tex",
                 light=False):
        """Génère le code latex décrivant la saé en utilisant le template latex donné
        dans ``modele``
        """
        if not light:
            modlatex = modeles.get_modele(modele)  # "templates/modele_ressource.tex")
        else:
            modlatex = modeles.get_modele(modele_light) # le modele en version réduite

        # La liste des compétences, des acs (et des coeffs)
        latex_competences = self.to_latex_competences_et_acs()
        if self.acs_optionnels:
            latex_acs_optionnels = self.to_latex_acs_optionnels()
        else:
            latex_acs_optionnels = ""
        # Préparation des ressources
        latex_ressource = self.to_latex_liste_fiches(self.yaml["ressources"])

        # Préparation des objectifs
        objectifs = self.yaml["objectifs"]
        if not objectifs:
            latex_objectifs = ""
        else:
            latex_objectifs = rpn.latex.md_to_latex(objectifs, self.officiel.DATA_MOTSCLES)

        # préparation de la description générique
        latex_descriptif = self.prepare_descriptif()

        # Prépare les parcours
        latex_parcours = self.yaml["parcours"]

        # Prépare les infos sur le cursus
        latex_cursus = self.prepare_cursus()

        # Prépare les coefficients
        latex_coeff = self.to_latex_prepare_coeffs()

        # Prépare les heures des fiches
        latex_heures_acd = self.prepare_heures("acd")
        latex_heures_gt_but = self.prepare_heures("gt-but")
        latex_heures_pn = self.prepare_heures("pn")

        # Injection dans le template
        chaine = modeles.TemplateLatex(modlatex).substitute(
            codelatex=self.get_code_latex_hyperlink(self.code),
            code=self.codeOReBUT, # self.code,
            codeRT=self.codeRT,
            url=rpn.latex.md_to_latex(self.yaml["url"] if "url" in self.yaml else "", self.officiel.DATA_MOTSCLES),
            nom=self.prepare_nom(),
            cursus=latex_cursus,
            heures_acd=latex_heures_acd,
            heures_pn=latex_heures_pn,
            heures_gt_but=latex_heures_gt_but,
            heures_projet_acd=self.heures["acd"]["projet"] if isinstance(self.heures["acd"]["projet"], int) else "???",
            heures_projet_gt_but=self.heures["gt-but"]["projet"] if isinstance(self.heures["gt-but"]["projet"], int) else "???",
            parcours=latex_parcours,
            coefficients=latex_coeff,
            objectifs=rpn.latex.nettoie_latex(latex_objectifs, self.officiel.DATA_ABBREVIATIONS),
            description=rpn.latex.nettoie_latex(latex_descriptif, self.officiel.DATA_ABBREVIATIONS),
            competences_et_acs=latex_competences,
            acs_optionnels = latex_acs_optionnels,
            listeRessources=latex_ressource
            # listeExemples = A FAIRE
        )
        # chaine = chaine.replace("&", "\&")
        # Ajoute à la chaine les exemples
        if self.exemples and not light: # le tableau donnant la liste des exemples
            chaine += "\n"
            chaine += self.to_latex_tableau_exemples()

            contenu = []
            for (i, e) in enumerate(self.exemples):
                contenu.append(e.to_latex(i+1))
            chaine += "\n\n".join(contenu)

        if self.realisations and not light: # les réalisations
            chaine += "\n"
            contenu = []
            for (i, rea) in enumerate(self.realisations):
                contenu.append(rea.to_latex(i + 1))
            chaine += "\n\n".join(contenu)
        return chaine

    def to_latex_tableau_exemples(self):
        chaine = ""
        if self.exemples:
            chaine += "\\begin{tabular}[t]{|P|T|}\n"
            chaine += "\\hline\n"
            chaine += "\\begin{tabular}[t]{@{}P@{}}\n"
            chaine += " \\bfseries \\textcolor{saeC}{Exemples de} \\tabularnewline\n"
            chaine += " \\bfseries \\textcolor{saeC}{mise en \oe{}uvre}\n"
            chaine += "\end{tabular}\n"
            chaine += " & \n"
            chaine += ""
            chaine += "\\begin{tabular}[t]{@{}T@{}}\n"
            aff = []
            for (i, e) in enumerate(self.exemples):
                aff.append(e.get_titre_numerote(i+1, option="hyperlink"))
            for (i, rea) in enumerate(self.realisations):
                aff.append(rea.get_titre_numerote(i+1, option="hyperlink"))
            chaine += "\n\\tabularnewline\n".join(aff)
            chaine += "\n\\tabularnewline\n"
            chaine += "\\end{tabular}\n"
            chaine += "\\\\\n"
            chaine += "\\hline\n"
            chaine += "\\end{tabular}\n"
        return chaine

    def prepare_descriptif(self):
        """Prépare le descriptif, avec dans le cas des SAEs de BUT1, fusion des livrables et des mots-clés"""

        champs = []

        descriptif = self.yaml["description"]
        latex_description = ""
        if descriptif:
            latex_description = rpn.latex.md_to_latex(descriptif, self.officiel.DATA_MOTSCLES)

        # préparation des livrables
        livrables = self.yaml["livrables"]
        latex_livrables = ""
        if livrables and livrables != "Aucun":
            latex_livrables = self.to_latex_champ_titre("Type de livrables ou de productions", self.yaml["livrables"])

        # préparation des prolongements
        latex_prolongements = ""
        if "prolongements" in self.yaml and self.yaml["prolongements"]:
            latex_prolongements = self.to_latex_champ_titre("Prolongements possibles", self.yaml["prolongements"] + ".")

        # préparation des mots-clés
        latex_mots = ""
        if self.yaml["motscles"]:
            mots = self.yaml["motscles"]
            if mots[-1] != ".":
                mots = mots + "."
            latex_mots = self.to_latex_champ_titre("Mots-clés",  mots)

        if not latex_livrables and not latex_mots:
            return latex_description
        else:
            champs = []
            if latex_description:
                latex_description = self.to_latex_champ_titre("Description", self.yaml["description"])
                champs.append(latex_description)
            if latex_livrables:
                champs.append(latex_livrables)
            if latex_prolongements:
                champs.append(latex_prolongements)
            if latex_mots:
                champs.append(latex_mots)
            return "\n\n\\vspace{0.2cm}\n".join(champs)

    def to_xml_prepare_descriptif(self):
        """Prépare le descriptif, avec dans le cas des SAEs de BUT1, fusion des livrables et des mots-clés"""

        champs = []

        # Description
        latex_description = self.yaml["description"]

        # Livrables
        livrables = self.yaml["livrables"]
        latex_livrables = ""
        if False: # ne met pas les livrables à la demande de la CCN

            if livrables and livrables != "Aucun":
                latex_livrables = "**Type de livrables ou de productions** : \n\n"
                latex_livrables += self.yaml["livrables"]

        # préparation des prolongements
        latex_prolongements = ""
        if "prolongements" in self.yaml and self.yaml["prolongements"]:
            latex_prolongements = "**Prolongements possibles** : \n\n"
            latex_prolongements += self.yaml["prolongements"] + "."

        # préparation des mots-clés
        latex_mots = ""
        if self.yaml["motscles"]:
            latex_mots = "**Mots-clés** : \n\n"
            latex_mots += self.yaml["motscles"] + "."

        if not latex_livrables and not latex_mots:
            return latex_description
        else:
            champs = []
            if latex_description:
                latex_description = "**Description** : \n\n"
                latex_description += self.yaml["description"]
                champs.append(latex_description)
            if False and latex_livrables:
                champs.append(latex_livrables)
            if latex_prolongements:
                champs.append(latex_prolongements)
            if latex_mots:
                champs.append(latex_mots)
            return "\n\n".join(champs)

    def to_xml(self, donnees_xml):
        """Ajoute la description de la sae au donnees_xml"""
        print(f"Export de {self.codeRT}/{self.code}/{self.codeOReBUT}")
        donnees_sae = ET.SubElement(donnees_xml, "sae")
        donnees_sae.set("code", self.codeOReBUT.replace("É", "E"))
        donnees_sae.set("ordre", str(self.ordre)) # ordre dans le semestre
        donnees_sae.set("heuresEncadrees", str(self.heures["gt-but"]["encadrees"])) # heures encadrées
        donnees_sae.set("heuresTP", str(self.heures["gt-but"]["tp"])) # heures tp
        donnees_sae.set("heuresPTUT", str(self.heures["gt-but"]["projet"])) # heures td
        # Titre
        titre_sae = ET.SubElement(donnees_sae, "titre")
        titre_sae.text = self.nom
        # Objectifs
        objectifs_sae = ET.SubElement(donnees_sae, "objectifs")
        obj = self.yaml["objectifs"]
        obj = rpn.latex.remplace_id_par_code_orebut(obj)
        objectifs_sae.text = "\n" + obj + "\n"
        # Description
        description_sae = ET.SubElement(donnees_sae, "description")
        descr = self.to_xml_prepare_descriptif()
        descr = rpn.latex.remplace_id_par_code_orebut(descr)
        description_sae.text = "\n" + descr + "\n"
        # ACs (les acs optionnels ne sont pas publiés)
        self.to_xml_ajoute_acs(donnees_sae)
        # Compétences
        self.to_xml_ajoute_competences(donnees_sae)
        # les ressources supports
        self.to_xml_ajoute_liste_activites(donnees_sae, self.yaml["ressources"], chapeau="ressources", champ="ressource")
        # Parcours
        self.to_xml_ajoute_parcours(donnees_sae)
        return

    def to_bdd(self, connexion, numero_semestre):
        """Ajoute la sae à la BDD pointée par la BDD"""

        # Ajout en tant qu'activité
        objectifs = self.yaml["objectifs"]
        if objectifs:
            objectifs = rpn.latex.remplace_id_par_code_orebut(objectifs)
        else:
            objectifs = ""
        description = self.yaml["description"]
        if description:
            description = rpn.latex.remplace_id_par_code_orebut(description)
        else:
            description = ""

        id_activite = self.to_bdd_activite(connexion, numero_semestre, self.codeOReBUT,
                                           self.nom, self.ordre, "", objectifs, description)

        # Ajoute les éléments spécifiques à la sae
