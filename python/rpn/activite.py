import ruamel.yaml
import logging

import rofficiel.officiel
import rpn.latex
import lxml.etree as ET
import sqlite3

__LOGGER = logging.getLogger(__name__)

MODALITES = ["CM/TD", "TP", "Projet"]   # modalités de mise en oeuvre d'une ressource/SAE


class ActivitePedagogique():
    """Modélise les éléments de bases d'une activité pédagogique (ressource, SAE ou exemple
    de SAE) et stocke les données officielles.

    Une *activité pédagogique* est initialisée par lecture des données contenues dans fichier yaml (attribut yaml).
    Ses attributs sont :

    * ``yaml`` : le dictionnaire lu dans le fichier yaml décrivant l'activité
    * ``code`` : le code de l'activité (par ex: ``"R101"`` ou ``"SAE12"``)
    * ``nom_semestre`` : le nom du semestre où se tient l'activité (par ex: ``"S1"``)
    * ``numero_semestre`` : le numéro du semestre où se tient l'activité (1, 2, ..., 6)
    * ``officiel`` : les données officielles du templates (cf. module ``officiel``)
    """
    __LOGGER = logging.getLogger(__name__)

    def __init__(self, fichieryaml, pnofficiel):
        """
        Charge les données du ``fichieryaml``
        """
        with open(fichieryaml, "r", encoding="utf8") as fid:
            yaml = ruamel.yaml.YAML()
            try:
                self.yaml = yaml.load(fid.read())
            except:
                ActivitePedagogique.__LOGGER.warning(f"Pb de chargement de {fichieryaml}")
        # Rapatrie les infos communes (code/titre)
        self.code = self.yaml["code"]
        self.codeRT = self.yaml["codeRT"]
        self.nom_semestre = self.yaml["semestre"]
        self.annee = None # <- chargé plus tard
        self.adaptation = None
        self.acs = None # <- chargé plus tard
        self.acs_optionnels = None
        self.parcours = None

        # Heures
        # self.heures_formation = None    # les heures de formation idéale
        # self.heures_formation_pn = None
        # self.details_heures_formation = {"cm": None, "tp": None, "td": None} # les heures de formation
        # self.details_heures_formation_pn = {"cm": None, "tp": None, "td": None}  # les heures de formation moins les 30% d'adaption locale
        # self.heures_projet = None
        # self.heures_projet_pn = None
        self.heures = None

        # Charges les données officielles
        self.officiel = pnofficiel

    def est_tronc_commun(self):
        """
        Indique si l'activité est de tronc commun
        :return:
        """
        if "Tronc" in self.parcours:
            return True
        # if len(self.parcours) == len(rofficiel.officiel.PARCOURS):
        #    return True
        return False


    def est_publie(self):
        """Indique si une activité est publiée ou non : ne sont pas publiées,
         les SAE, les ressources complémentaires"""
        if self.est_sae_from_code():
            return False
        elif self.type["complementaire"]:
            return False
        else:
            return True


    def est_ressource_from_code(self):
        """Indique si l'activité est une ressource d'après le code"""
        if "R" in self.code:
            return True
        else:
            return False

    def est_sae_from_code(self):
        """Indique si l'activité est une sae d'après le code"""
        if "SA" in self.code:
            return True
        else:
            return False


    def getInfo(self):
        """Renvoie les données du yaml (pour export to html)"""
        return self.yaml


    def prepare_nom(self):
        """Prépare le nom d'une activité"""
        return rpn.latex.nettoie_latex(self.nom.replace("&", "\\&"), self.officiel.DATA_ABBREVIATIONS)


    def prepare_cursus(self):
        """Prépare les informations sur le cursus"""
        # Prépare cursus
        champs = []
        if isinstance(self.annee, str):
            champs.append(self.annee)
        else:
            champs.append(", ".join(self.annee))
        if isinstance(self.nom_semestre, str):
            champs.append(self.nom_semestre)
        else:
            champs.append(", ".join(self.nom_semestre))
        if self.adaptation:
            champs.append("\\textit{Adaptation locale}")
        if self.type["complementaire"]:
            champs.append("\\textit{Ressource complémentaire}")
        return " > ".join(champs)


    def prepare_heures(self, publication):
        """Prépare l'affichage latex des heures pour une publication donnée ("acd", "pn", "gt-but")
        """
        if publication == "pn" and not self.est_publie():
            chaine = "\\textbf{non publié}"
        else:
            chaine = ""
            if isinstance(self.heures[publication]["encadrees"], int):
                chaine += "\\textbf{%dh}" % (self.heures[publication]["encadrees"])
            else:
                self.__LOGGER.error(f"{self}: prepare_heures: nombre d'heures encadrées (totales) manquant")
                chaine += "\\textbf{???}"

            champs = []

            if isinstance(self.heures[publication]["cm"], int) and isinstance(self.heures[publication]["td"], int):
                for type in ["cm", "td"]:
                    champs.append("%dh %s" % (self.heures[publication][type], type.upper()))
            elif isinstance(self.heures[publication]["cm/td"], int):

                champs.append("%dh %s" % (self.heures[publication]["cm/td"],
                                              "cm/td".upper()))
            # ajout des TP
            if isinstance(self.heures[publication]["tp"], int):
                champs.append("%dh %s" % (self.heures[publication]["tp"], "tp".upper()))
            else:
                champs.append("???h TP")

            # injection des champs
            chaine += ", dont "
            if len(champs) > 1:
                chaine += ", ".join(champs[:-1]) + " et " + champs[-1]
            else:
                chaine += champs[0]
        return chaine

    def tous_les_acs_de_toutes_les_competentes(self):
        """Détecte si tous les acs de toutes les compétences sont cochés (tronc commun + parcours)"""
        tous = True
        les_comp_possibles = self.officiel.DATA_ACS[self.annee]
        for comp in les_comp_possibles: # toutes les comp
            if comp in self.acs: # si la comp est dans les ACs
                for acs in self.officiel.DATA_ACS[self.annee][comp]: # pour tous les ACs de la comp
                    if acs not in self.acs[comp]:
                        tous = False # si on trouve un acs non présent
            else: # si la comp n'est pas dans les ACS
                tous = False
        return tous

    def tous_les_acs_de_toutes_les_competentes_d_autres_parcours(self, parcours_exclu):
        """Détecte si tous les acs de toutes les compétences de parcours autres que parcours_exclus
        sont cochés (les comp de TC n'étant pas prises en compte)"""
        tous = True
        les_comp_possibles = [comp for comp in self.officiel.DATA_ACS[self.annee] if parcours_exclu not in comp and "RT" not in comp] # les comp autres
        for comp in les_comp_possibles: # toutes les comp
            if comp in self.acs_optionnels: # si la comp est dans les ACs
                for acs in self.officiel.DATA_ACS[self.annee][comp]: # pour tous les ACs de la comp
                    if acs not in self.acs_optionnels[comp]:
                        tous = False # si on trouve un acs non présent
            else: # si la comp n'est pas dans les ACS
                tous = False
        return tous

    def to_latex_liste_competences_et_acs_a_supprimer(self):
        """Renvoie la description latex d'une liste de compétences et d'acs"""


        latex_comp = []
        nbre_acs = sum([len(self.acs[comp]) for comp in self.acs])
        nbre_acs_total = sum([len(self.officiel.DATA_ACS[self.annee][comp]) for comp in self.officiel.DATA_ACS[self.annee]])


        # Nom compétence =>2 colonnes "\\begin{tabular}{UW}\n"
        # mapping ACs de la ressource -> compétences
        for comp in self.acs:
            details_competences = []
            # \hyperlink{comp:\compcode}{\textcolor{saeC}{\saecode}}
            # le nom de la comp
            type_niveau = "\\niveau" + {"BUT1": "A", "BUT2": "B", "BUT3": "C"}[self.yaml["annee"]]

            if "RT" in comp:
                couleur = "compC" + ["A", "B", "C"][int(comp[-1])-1]
            else:
                couleur = "compS" + ["A", "B"][int(comp[-1])-1] # la couleur pour les parcours
            nom_comp = self.officiel.DATA_COMPETENCES[comp].replace("&","\\&")
            details_competences.append("\\textcolor{%s}{%s} & %s" % (couleur, comp, nom_comp)) # le code de la comp
            # details_competences.append("\\tabularnewline")

            # Préparation du coeff (si existe)
            # ajoutcoeff = "\\ajoutRcoeff{%s}"
            # details_competences.append(ajoutcoeff % (str(self.ressource["coeffs"][comp])))

            # Préparation des ac
            details_acs = []
            for code_ac in self.acs[comp]:  # les acs de la ressource (triés théoriquement)
                if code_ac not in self.officiel.DATA_ACS[self.annee][comp]:
                    self.__LOGGER.warning(f"{self.code}/{self.codeRT}: to_latex: Pb {code_ac} non trouvé en {self.annee}")
                else:
                    nom_acs = " & " + "\\textcolor{%s}{%s}" % (couleur, code_ac)
                    nom_acs += " \\textit{%s}" % (self.officiel.DATA_ACS[self.annee][comp][code_ac].replace("&","\\&"))
                    details_acs.append(nom_acs)
            details_competences.append("\n\\tabularnewline\n".join(details_acs))

            # toutes les infos sur la comp
            latex_comp.append("\n\\tabularnewline\n".join(details_competences))

        # le latex final
        if latex_comp:
            chaine = "\\begin{tabular}[t]{UW}\n"
            chaine += "\n\\tabularnewline\n".join(latex_comp)
            chaine += "\n\\tabularnewline\n"
            chaine += "\\end{tabular}"
        else:
            chaine = ""


        return chaine


    def to_latex_champ_titre(self, titre, champ):
        """Convertit un champ en latex en lui ajoutant un titre"""
        latex_champ = ""
        if champ:
            champs = []
            champs.append("{\\bfseries " + titre + "}")
            champs.append(rpn.latex.md_to_latex(champ, self.officiel.DATA_MOTSCLES))
            latex_champ = "\n\n".join(champs)
        return latex_champ

    def to_latex_competences_et_acs(self):
        """Renvoie la description latex d'une liste de compétences et d'acs"""
        return self.to_latex_tableau_competences_et_acs(self.acs)


    def to_latex_tableau_competences_et_acs(self, acs, type="obligatoire"):
        """Renvoie la description latex d'une liste de compétences et d'acs"""
        latex_comp = []

        if acs and self.annee in ["BUT1", "BUT2", "BUT3"]:
            # nbre_acs = sum([len(acs[comp]) for comp in acs])
            #  nbre_acs_total = sum([len(self.officiel.DATA_ACS[self.annee][comp]) for comp in self.officiel.DATA_ACS[self.annee]])

            # if nbre_acs == nbre_acs_total: # Tous les ACs
            #    return "\\textit{Tous les ACs de toutes les compétences (tous parcours confondus)}"
            if type == "obligatoire" and self.tous_les_acs_de_toutes_les_competentes():
                return "\\textit{Tous les ACs de toutes les compétences (tout parcours confondu)}"
            elif type != "obligatoire" and self.parcours != "Tronc commun" and \
                    self.tous_les_acs_de_toutes_les_competentes_d_autres_parcours(self.parcours):
                les_parcours = [p for p in rofficiel.officiel.PARCOURS if p != self.parcours]
                self.__LOGGER.warning(f"{self.code}/{self.codeRT} Tous les ACs des compétences des autres parcours : " + ", ".join(les_parcours) )
                return "\\textit{Tous les ACS des compétences des parcours " + ", ".join(les_parcours) + "}"

        # mapping ACs de la ressource -> compétences
        for comp in acs:
            details_competences = []
            # \hyperlink{comp:\compcode}{\textcolor{saeC}{\saecode}}
            # le nom de la comp
            type_niveau = "\\niveauA"
            if isinstance(self.yaml["annee"], str):
                type_niveau = "\\niveau" + {"BUT1": "A", "BUT2": "B", "BUT3": "C"}[self.yaml["annee"]]

            if "RT" in comp:
                couleur = "compC" + ["A", "B", "C"][int(comp[-1])-1]
            else:
                couleur = "compS" + ["A", "B"][int(comp[-1])-1] # la couleur pour les parcours
            nom_comp = self.officiel.DATA_COMPETENCES[comp].replace("&", "\\&")
            details_competences.append("\\textcolor{%s}{%s} %s" % (couleur, comp, nom_comp)) # le code de la comp
            # details_competences.append("\\tabularnewline")

            # Préparation du coeff (si existe)
            # ajoutcoeff = "\\ajoutRcoeff{%s}"
            # details_competences.append(ajoutcoeff % (str(self.ressource["coeffs"][comp])))

            # Affichage du niveau de la compétence (si pas ressource complémentaire)
            details_acs = []
            for code_ac in acs[comp]:  # les acs de la ressource (triés théoriquement)
                if not self.type["complementaire"] and code_ac not in self.officiel.DATA_ACS[self.annee][comp]:
                    self.__LOGGER.warning(f"{self.code}/{self.codeRT}: to_latex: Pb {code_ac} non trouvé en {self.annee}")
                else:
                    nom_acs = "\\textcolor{%s}{$\\bullet$ %s}" % (couleur, code_ac)
                    annee = self.officiel.get_annee_from_acs(code_ac)
                    intitule = self.officiel.DATA_ACS[annee][comp][code_ac].replace("&", "\\&")
                    nom_acs += " \\textit{%s}" % intitule
                    details_acs.append(nom_acs)
            details_competences.append("\n\\tabularnewline\n".join(details_acs))


            # toutes les infos sur la comp
            latex_comp.append("\n\\tabularnewline\n".join(details_competences))

        # le latex final
        if latex_comp:
            chaine = "\\begin{tabular}[t]{@{}T@{}}\n"
            chaine += "\n\\tabularnewline\n".join(latex_comp)
            chaine += "\n\\tabularnewline\n"
            chaine += "\\end{tabular}\n"
        else:
            chaine = ""
        return chaine


    def to_latex_acs_optionnels(self):
        """Renvoie la description latex d'une liste de compétences et d'acs"""
        """Renvoie la description des acs optionnels"""

        latex_comp = []
        if not self.type["complementaire"]:
            nbre_acs = sum([len(self.acs_optionnels[comp]) for comp in self.acs_optionnels])
            nbre_acs_total = sum([len(self.officiel.DATA_ACS[self.annee][comp])
                                  for comp in self.officiel.DATA_ACS[self.annee]
                                  if not comp.startswith("RT")]) # le nbre dacs de parcours
            ts_acs_opt = (nbre_acs == nbre_acs_total)
        else:
            ts_acs_opt = False
        if self.code.startswith("R"):
            couleur = "ressourceC"
        else:
            couleur = "saeC"

        if ts_acs_opt: # Tous les ACs
            latex = ""
            latex += "\\begin{tabular}[t]{@{}P@{}}\n"
            latex += "\\bfseries \\bfseries \\textcolor{%s}{Apprentissages} \\tabularnewline\n" % (couleur)
            latex += "\\bfseries \\textcolor{%s}{critiques} \\tabularnewline\n" % (couleur)
            latex += "\\bfseries \\textcolor{%s}{optionnels}\n" % (couleur)
            latex += "\\end{tabular}\n"
            latex += "&\n"
            latex += "\\textit{Tous les ACs des compétences de parcours} \\\\ "
            latex += "\\hline \n"
            return latex

        if self.yaml["acs_optionnels"]:
            latex = ""
            latex += "\\begin{tabular}[t]{@{}P@{}}\n"
            latex += "\\bfseries \\bfseries \\textcolor{%s}{Apprentissages} \\tabularnewline\n" % (couleur)
            latex += "\\bfseries \\textcolor{%s}{critiques} \\tabularnewline\n" % (couleur)
            latex += "\\bfseries \\textcolor{%s}{optionnels}\n" % (couleur)
            latex += "\\end{tabular}\n"
            latex += "&\n"
            latex += self.to_latex_tableau_competences_et_acs(self.yaml["acs_optionnels"], type="optionnel") + "\n"
            latex += " \\\\ \\hline\n"
            return latex
        else:
            return ""

    def to_latex_liste_fiches(self, liste_fiches):
        """Prépare le latex d'une liste de fiches (fournis en paramètre) = ressources ou SAE indifféremment
        ex: liste_fiches = self.yaml["saes"]
        -> equivalent de \listeSAE"""
        latex_liste = []

        for elmt in liste_fiches:
            if elmt.startswith("S"):
                couleur = "saeC"
                intitule = self.officiel.get_sae_name_by_code(elmt)

            else:
                couleur = "ressourceC"
                intitule = self.officiel.get_ressource_name_by_code(elmt)

            if intitule:
                intitule = intitule.replace("&", "\\&") # échappement des &
            else:
                intitule = ""

            code_orebut = self.officiel.get_code_orebut_from_code(elmt)
            if not code_orebut:
                self.__LOGGER.warning(f"to_latex_liste_fiches: Pb code orebut manquant pour {elmt}")
            if False:
                nom = "\\hyperlink{%s}{\\textcolor{%s}{%s}} & %s" % (self.get_code_latex_hyperlink(elmt),
                                                                     couleur, code_orebut, intitule)
            else:
                nom = "\\hyperlink{%s}{\\textcolor{%s}{%s}} %s" % (self.get_code_latex_hyperlink(elmt),
                                                                   couleur, code_orebut, intitule)

            latex_liste.append(nom)

        if latex_liste:
            if False:
                chaine = "\\begin{tabular}[t]{@{}UW@{}}\n"
            else:
                chaine = "\\begin{tabular}[t]{@{}T@{}}\n"
            chaine += "\n\\tabularnewline\n".join(latex_liste)
            chaine += "\n\\tabularnewline\n"
            chaine += "\\end{tabular}"
        else:
            chaine = ""
        return chaine


    def get_code_latex_hyperlink(self, contenu):
        return contenu.replace(".", "").replace("É", "E").replace(" ", "")


    def to_xml_ajoute_acs(self, donnees_xml):
        """Ajoute la liste des acs à un élément xml"""
        acs_sae = ET.SubElement(donnees_xml, "acs")  # liste des ac
        for comp in self.acs:
            for ac in self.acs[comp]:
                ac_sae = ET.SubElement(acs_sae, "ac")
                ac_sae.text = ac


    def to_xml_ajoute_competences(self, donnees_xml):
        """Ajoute la liste des compétences à un élément xml"""
        # Compétences
        competences_sae = ET.SubElement(donnees_xml, "competences")

        for comp in self.acs:
            comp_sae = ET.SubElement(competences_sae, "competence")
            if self.coeffs:
                if comp not in self.coeffs:
                    self.__LOGGER.error(f"Coeff manquant pour {comp} {self.code}/{self.codeRT}")
                    coeff = 0
                else:
                    coeff = self.coeffs[comp]
            else:
                self.__LOGGER.error(f"Pas de coeffs du tout pour {self.code}/{self.codeRT}")
                coeff = 0
            comp_sae.set("coefficient", str(coeff)) # <= TODO: A remplacer par le coeff
            diminutif = self.officiel.get_diminutif_comp_from_code_comp(comp)
            comp_sae.set("nom", diminutif)


    def to_xml_ajoute_parcours(self, donnees_xml):
        """Ajoute la liste des parcours à un élément xml"""
        parcours_xml = ET.SubElement(donnees_xml, "liste_parcours")
        if self.parcours == 'Tronc commun':
            les_parcours = rofficiel.officiel.PARCOURS
        else:
            les_parcours = [ self.parcours ]
        for p in les_parcours:
            p_xml = ET.SubElement(parcours_xml, "parcours")
            p_xml.text = p


    def to_xml_ajoute_liste_activites(self, donnees_xml,
                                      activites, chapeau="ressources", champ="ressource"):
        """Ajoute une liste d'activités (ressources ou saes)
        avec un chapeau (par ex: prerequis) incluant des champs"""
        liste_xml = ET.SubElement(donnees_xml, chapeau)
        for res in activites:
            code_orebut = self.officiel.get_code_orebut_from_code(res)
            elmt_xml = ET.SubElement(liste_xml, champ)
            elmt_xml.text = code_orebut
        if len(activites) == 0:
            liste_xml.text = " "

    def to_latex_prepare_coeffs(self):
        """Prépare l'affichage des coefficients"""
        latex = []
        for comp in self.officiel.DATA_COMPETENCES:
            if "RT" in comp:
                couleur = "compC" + ["A", "B", "C"][int(comp[-1])-1]
            else:
                couleur = "compS" + ["A", "B"][int(comp[-1])-1] # la couleur pour les parcours
            if self.coeffs and comp in self.coeffs:
                cof = self.yaml["coeffs"][comp]
                latex += [ "\\textcolor{%s}{%s}~:~coeff.~\\textbf{%s}" % (couleur, comp, str(cof))]
        return "\\hspace*{0.5cm}".join(latex)


    def to_bdd_activite(self, connexion,
                        numero_semestre, codeOreBUT, nom, ordre,
                        diminutif, contexte, description):
        """Insère l'activité dans la BDD pointée par connexion"""

        # Insertion de l'activite
        contexte = rpn.latex.echappe_guillement(contexte)
        description = rpn.latex.echappe_guillement(description)

        requete = f"""INSERT INTO activites 
                      (code, id_semestre, ordre, nom, diminutif, contexte, description)
                    VALUES ("{codeOreBUT}", {numero_semestre}, {ordre}, "{nom}", 
                            "{diminutif}", "{contexte}", "{description}")
                    """
        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            id_activite = curseur.lastrowid # l'id du dernier inséré
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.warning(f"activite.to_bdd_activite: Activite {codeOreBUT} déjà insérée")
            id_activite = codeOreBUT # self.get_id_activite_from_bdd(connexion, codeOreBUT)
        except sqlite3.Error as e:
            id_activite = None
            self.__LOGGER.error("activite.to_bdd_activite: Insertion de l'activité non réalisé")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")
            return

        # Insère les acs obligatoire
        for ac in self.acs:
            self.to_bdd_lien_acs_activite(connexion, ac, codeOreBUT, False)
        for ac in self.acs_optionnels:
            self.to_bdd_lien_acs_activite(connexion, ac, codeOreBUT, True)
        return id_activite

    def to_bdd_lien_acs_activite(self, connexion, ac, codeOreBUT, optionnel=False):
        """Les acs liés à l'activité"""
        # récupère le code de l'AC
        requete = f"""SELECT id FROM acs WHERE code="{ac}"
                    """
        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            id_ac = curseur.fetchone()[0]
            connexion.commit()
        except:
            id_ac = None
            self.__LOGGER("activite.to_bdd_lien_acs_activite : ac {ac} non trouvé")
            return id_ac

        # Insertion du lien
        requete = f"""INSERT INTO coche (id_ac, code_activite, optionnel)
                      VALUES ({id_ac}, "{codeOreBUT}", {optionnel})"""
        try:
            curseur.execute(requete)
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.warning(f"activite.to_bdd_lien_acs_activite: Coche {ac}/{codeOreBUT} déjà insérée")
        except sqlite3.Error as e:
            self.__LOGGER.error(f"activite.to_bdd_lien_acs_activite: Coche {ac}/{codeOreBUT} non réalisé")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")
            return

