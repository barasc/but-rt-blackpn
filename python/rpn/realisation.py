import logging

import rpn.latex, modeles
from config import Config
import rpn.activite



class RealisationSAE(rpn.activite.ActivitePedagogique):
    """Une réalisation d'une SAE"""
    __LOGGER = logging.getLogger(__name__)
    BASE = "https://barasc.gricad-pages.univ-grenoble-alpes.fr/but-rt-blackpn/"

    def __init__(self, fichieryaml, officiel):
        super().__init__(fichieryaml, officiel)
        self.nom = self.yaml["titre"]
        self.auteur = self.yaml["auteur"]
        self.iut = self.yaml["iut"]

    def get_titre_numerote(self, numero, option=None):
        """Renvoie un titre numéroté de la forme
        Réalisation X : titre (ou Réalisation X si pas de titre) ;
        """
        intitule = "Réalisation %d" % (numero)
        if option == "hyperlink":
            titre = "\\hyperlink{%sRea%d}{%s}" % (self.get_code_latex_hyperlink(self.code), numero, intitule)
        elif option == "hypertarget":
            titre = "\\hypertarget{%sRea%d}{%s}" % (self.get_code_latex_hyperlink(self.code), numero, intitule)
        else:
            titre = intitule
        if self.nom:
            titre += " : " + self.nom.replace("&", "\\&")
        return titre

    def get_titre(self, numero, option=None):
        """Renvoie un titre numéroté de la forme
        Réalisation X : titre (ou Réalisation X si pas de titre) ;
        """
        # intitule = "Réalisation %d" % (numero)
        intitule = ""
        if option == "hyperlink":
            titre = "\\hyperlink{%sRea%d}{%s}" % (self.get_code_latex_hyperlink(self.code), numero, intitule)
        elif option == "hypertarget":
            titre = "\\hypertarget{%sRea%d}{%s}" % (self.get_code_latex_hyperlink(self.code), numero, intitule)
        else:
            titre = intitule
        if self.nom:
            titre += self.nom.replace("&", "\\&")
        return titre

    def to_latex(self, numero,
                 modele="templates/modele_tableau_realisation.tex"):
        """Génère le code latex décrivant un exemple de SAE en utilisant le template
        donné dans ``modele``
        """
        modlatex = modeles.get_modele(modele)  # "templates/modele_ressource.tex")

        # préparation du titre

        titre = "{\\bfseries " + self.get_titre(numero, option="hypertarget") + "}"

        # préparation du descriptif
        description = self.prepare_description()
        documents = self.prepare_document()
        chaine = ""
        chaine = modeles.TemplateLatex(modlatex).substitute(
            numero=str(numero),
            titre=titre,  # remplacement & par \& déjà fait
            auteur=self.auteur,
            iut=self.iut.replace("&", "\&"),
            description=rpn.latex.nettoie_latex(description, self.officiel.DATA_ABBREVIATIONS),
            documents=documents
        )
        # chaine = chaine.replace("&", "\&")

        return chaine


    def prepare_description_old(self):
        """Prépare la description"""

        # préparation de la problématique
        description = self.yaml["description"]
        latex_description = ""
        if description:
            latex_description = rpn.latex.md_to_latex(description, self.officiel.DATA_MOTSCLES)

        # préparation des liens vers des documents
        latex_documents = ""
        if "documents" in self.yaml:
            documents = self.yaml["documents"]
            champs_documents = []
            liste_documents = []
            for doc in documents: # liste de liens ou de doc
                if doc.startswith("exemples/"): # un lien local
                    fichier = doc.split("/")[-1] # le nom du fichier
                    liste_documents.append("\\href{%s%s}{%s}" % (RealisationSAE.BASE, doc,
                                                                 fichier.replace("_", "\\_")))
            if liste_documents:
                champ = "\\begin{itemize}\n"
                champ += "\\item "
                champ += "\n\\item ".join(liste_documents)
                champ += "\n\\end{itemize}"
                champs_documents = ["{\\bfseries Documents }", champ]
            latex_documents = "\n\n".join(champs_documents)

        champs = []
        if latex_description:
            champs += [latex_description]
        if latex_documents:
            champs += [latex_documents]
        return "\n\n\\vspace{0.2cm}\n".join(champs)

    def prepare_description(self):
        """Prépare la description"""

        # préparation de la problématique
        description = self.yaml["description"]
        latex_description = ""
        if description:
            latex_description = rpn.latex.md_to_latex(description, self.officiel.DATA_MOTSCLES)
        return latex_description

    def prepare_document(self):
        # préparation des liens vers des documents
        latex_documents = ""
        if "documents" in self.yaml:
            documents = self.yaml["documents"]
            champs_documents = ""
            liste_documents = []
            for doc in documents: # liste de liens ou de doc
                if doc.startswith("exemples/"): # un lien local
                    fichier = doc.split("/")[-1] # le nom du fichier
                    liste_documents.append("\\href{%s%s}{%s}" % (RealisationSAE.BASE, doc,
                                                                 fichier.replace("_", "\\_")))
            if liste_documents:
                champ = "\\begin{itemize}\n"
                champ += "\\item "
                champ += " \n\\item ".join(liste_documents)
                champ += "\n\\end{itemize}"
                champs_documents = champ # ["{\\bfseries Documents }", champ]
            latex_documents = champs_documents # "\n\n".join(champs_documents)

        return latex_documents

