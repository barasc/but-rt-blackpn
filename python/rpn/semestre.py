"""
Module semestre
"""
import glob, os, logging
import string

import rofficiel.officiel
import rpn.exemple, rpn.realisation
import rpn.ressource
import rpn.sae
from config import Config
import rpn.latex
import rpn.activite, modeles
import lxml.etree as ET

class SemestrePN():
    """
    Classe modélisant un semestre (ensemble de SAés et de ressources)

    Les ressources et les SAé sont chargées d’après les fichiers yaml,
    stockés dans des répertoires spécifiques.
    """
    __LOGGER = logging.getLogger(__name__)

    def __init__(self, nom_semestre,
                 repertoire_ressources,
                 repertoire_saes,
                 pnofficiel):
        """
        Modélise un semestre avec ses ressources et ses SAés, en utilisant les données
        officiels (yaml).

        """
        self.nom_semestre = nom_semestre
        self.numero_semestre = nom_semestre[1:]

        self.parcours = None # <- Mis à jour plus tard si besoin
        self.ressources = {}
        self.saes = {}
        self.exemples = {}
        self.realisations = {}

        # Charge les infos officielles
        self.officiel = pnofficiel
        self.annee = self.officiel.get_annee_from_semestre(self.numero_semestre)

        # Les ACS du semestre
        if not self.est_complementaire():
            self.comp_et_acs = self.officiel.DATA_ACS[self.annee] #
        else:
            self.comp_et_acs = {}
        self.nbre_acs = len([self.comp_et_acs[comp][a] for comp in self.comp_et_acs for a in self.comp_et_acs[comp]])

        # Les compétences du semestre
        if not self.est_complementaire():
            self.niveaux = self.officiel.get_noms_niveaux()[self.annee]
        else:
            self.niveaux = {}
        self.comp = self.officiel.DATA_COMPETENCES

        # Chargement des ressources
        self.get_activites_from_yaml(type="ressource",
                                     repertoire=repertoire_ressources + f"/{nom_semestre}")
        # Chargement des SAés et des exemples
        self.get_activites_from_yaml(type="saé",
                                     repertoire=repertoire_saes + f"/{nom_semestre}")

        # Les données numériques
        self.nbre_ressources = len(self.ressources)
        self.nbre_saes = len(self.saes)
        self.activites = {**self.saes, **self.ressources} # les saes et les ressources
        self.nbre_activites = len(self.activites)



        # Checks divers
        self.check_activites_vs_officiel()

        # Trie des fiches par parcours
        self.saes_par_parcours = self.tri_activites_par_parcours(self.saes)
        self.ressources_par_parcours = self.tri_activites_par_parcours(self.ressources)

        # Matrices des coeff par PARCOURS
        self.matrices_coefficients = {'TC': self.get_matrices_coefficients_par_parcours(parcours=None)}
        for p in rofficiel.officiel.PARCOURS:
            self.matrices_coefficients[p] = self.get_matrices_coefficients_par_parcours(parcours=p)


    def est_complementaire(self):
        """Indique si une activité (ressource a priori) est complémentaire ou non"""
        if self.annee == "BUTC":
            return True
        else:
            return False


    def tri_activites_par_parcours(self, dico):
        """Tri une liste d'activité par parcours (soit des ressources, soit des saes, donc les codes sont fournis dans un dictionnaire)"""
        tri = {p: {} for p in ["Tronc commun"] + rofficiel.officiel.PARCOURS} # + ["Fiches complémentaires"]}
        for code in dico:
            ajout = False
            a = dico[code]
            if a.est_tronc_commun():
                tri["Tronc commun"][code] = a
                ajout = True
            else:
                for p in rofficiel.officiel.PARCOURS:
                    if p in dico[code].yaml["parcours"]:
                        tri[p][code] = a
                        ajout = True
            if not ajout:
                self.__LOGGER.warning(f"{self.nom_semestre}: tri_activites_par_parcours: {code} n'a pas pu être ajouté à un parcours")
        return tri


    def get_niveau_from_comp(self, comp):
        """Renvoie le niveau (numéro) d'une comp en fonction de l'année => à déplacer"""
        if "RT" in comp:
            niveau = int(self.annee[-1]) # le niveau = l'année
        else:
            niveau = int(self.annee[-1]) - 1 # le niveau est décrémenté
        return niveau


    def get_activites_from_yaml(self,
                                type,
                                repertoire):
        """
        Charge les activités dont le type (ressources ou saes) est indiqué, rattachées au semestre nom_semestre dont
        les yaml sont dans le repertoire
        """
        fichiers_definitifs = [os.path.split(x)[1] for x in glob.glob(repertoire + '/*.yml')]
        fichiers = [repertoire + "/" + f for f in fichiers_definitifs]
        fichiers = sorted(fichiers)  # tri par ordre alphabétique
        # ne conserve que les fichiers du-dit semestre RN.XX ou N=numero du semestre

        for fichieryaml in fichiers:
            if type == "ressource":
                a = rpn.ressource.Ressource(fichieryaml, self.officiel)  # lecture du fichier
                if not self.est_complementaire():
                    if a.nom_semestre == self.nom_semestre:
                        self.ressources[a.code] = a
                else:
                    self.ressources[a.code] = a
            else: # type = "saé"
                if "exemple" not in fichieryaml and "realisation" not in fichieryaml:
                    a = rpn.sae.SAE(fichieryaml, self.officiel)
                    if not self.est_complementaire():
                        if a.nom_semestre == self.nom_semestre:
                            self.saes[a.code] = a
                    else:
                        self.saes[a.code] = a
                elif "exemple" in fichieryaml: # un exemple de SAE dans les fiches google
                    e = rpn.exemple.ExempleSAE(fichieryaml, self.officiel)
                    sae = e.yaml["code"]
                    if sae not in self.exemples:
                        self.exemples[sae] = []
                    self.exemples[sae].append(e)
                elif "realisation" in fichieryaml:
                    rea = rpn.realisation.RealisationSAE(fichieryaml, self.officiel)
                    sae = rea.yaml["code"]
                    if sae not in self.realisations:
                        self.realisations[sae] = []
                    self.realisations[sae].append(rea)
                else: # ??

                    print(f"{fichieryaml} non traité pour l'instant")


        # injecte les exemples dans les sae
        for s in self.exemples:
            if s not in self.saes:
                self.__LOGGER.warning(f"{self.nom_semestre}: la SAE {s} n'existe pas et ne peut être chargé avec ses exemples")
            else:
                for e in self.exemples[s]:
                    self.saes[s].charge_exemple(e)
        for s in self.realisations:
            if s not in self.saes:
                self.__LOGGER.warning(
                    f"{self.nom_semestre}: la SAE {s} n'existe pas et ne peut être chargé avec ses réalisations")
            else:
                for rea in self.realisations[s]:
                    self.saes[s].charge_realisation(rea)

        if type == "ressources":
            SemestrePN.__LOGGER.info("Semestre {} : {} ressources chargées".format(self.nom_semestre,
                          len(self.ressources)))
        else:
            SemestrePN.__LOGGER.info("Semestre {} : {} saés chargées, {} exemples et {} réalisations".format(
                                            self.nom_semestre,
                                            len(self.saes),
                                            sum([len(self.exemples[s]) for s in self.exemples]),
                                     sum([len(self.realisations[s]) for s in self.realisations])))



    def check_activites_vs_officiel(self):
        """Check si le nombre de ressources & de saés chargés correspond au
        nombre prévu dans les données officielles
        """
        nbre_saes_attendues = 0
        for p in self.officiel.DATA_SAES[self.nom_semestre]:
            nbre_saes_attendues += len(self.officiel.DATA_SAES[self.nom_semestre][p])
        nbre_ressources_attendues = 0
        for p in self.officiel.DATA_RESSOURCES[self.nom_semestre]:
            nbre_ressources_attendues += len(self.officiel.DATA_RESSOURCES[self.nom_semestre][p])
        if len(self.saes) != nbre_saes_attendues:
            SemestrePN.__LOGGER.warning(f"Pb => il manque des saes au {self.nom_semestre}")
        if len(self.ressources) != nbre_ressources_attendues:
            SemestrePN.__LOGGER.warning(f"Pb => il manque des ressources au {self.nom_semestre}")


    def tri_codes_competences(self, parcours=None):
        """Trie les compétences avec le tronc commun d'abord (RT1, RT2, RT3) puis les parcours.
        Si parcours=`None`, tous les parcours sont considérés ;
        Sinon seul le parcours mentionné est considéré"""
        if not parcours:
            comps = [c for c in self.comp_et_acs if not c.startswith("RT")] # les comps de parcours (tous parcours confondus)
        else:
            comps = [c for c in self.comp_et_acs if not c.startswith("RT") and parcours in c]
        return ["RT{}".format(i) for i in range(1, 4)] + sorted(comps)


    def tri_comp_et_acs(self, parcours=None):
        """Renvoie les codes des ACS triés par compétence (dictionnaire {competence: [acs]},
        tous parcours confondus
        si ``parcours=None`` et pour un parcours donné s'il est mentionné"""
        competences_triees = self.tri_codes_competences(parcours=parcours)
        comp_et_acs_du_parcours = {}
        for comp in competences_triees:
            comp_et_acs_du_parcours[comp] = sorted([ac for ac in self.comp_et_acs[comp]])
        return comp_et_acs_du_parcours


    def tri_liste_codes_acs(self, parcours=None):
        """La même mais pour une liste"""
        competences_triees = self.tri_codes_competences(parcours=parcours)
        acs_du_parcours = self.tri_comp_et_acs()
        return [ac for comp in competences_triees for ac in acs_du_parcours[comp]]


    def tri_codes_activites(self, codes_a_trier, parcours=None):
        """
        Renvoie les codes_a_trier triés par code croissant.
        Si parcours = None, tous les parcours sont considérés
        Si parcours mentionné, seul le parcours indiqué est pris en compte
        """
        liste_codes = sorted(codes_a_trier)
        if not parcours:
            return liste_codes
        else: # les ressources triées d'un parcours
            codes_du_parcours = []
            for c in liste_codes:
                a = self.activites[c]
                if a.est_tronc_commun():
                    codes_du_parcours.append(c)
                elif parcours in a.parcours:
                    codes_du_parcours.append(c)
            return codes_du_parcours


    def tri_codes_ressources(self, parcours=None):
        """
        Renvoie les codes des ressources triés par code croissant.
        Si parcours = None, tous les parcours sont considérés
        Si parcours mentionné, seul le parcours indiqué est pris en compte
        """
        return self.tri_codes_activites(self.ressources, parcours=parcours)


    def tri_codes_saes(self, parcours=None):
        """
        Renvoie les codes des SAés triés
        Si parcours = None, tous les parcours sont considérés
        Si parcours mentionné, seul le parcours indiqué est pris en compte
        :return:
        """
        return self.tri_codes_activites(self.saes, parcours=parcours)


    def get_matrices_dependances(self):
        """
        Renvoie la matrice traduisant les dépendances entre les saés et les
        ressources d’un même semestre
        La matrice contient None si aucune dépendance n'est trouvée, -1 si la sae s'appuie sur une ressource,
        1 si la ressource se prolonge sur la sae => 0 si les saes et les ressources sont bien synchrone
        """
        saes = self.tri_codes_saes() # les saes tries
        ressources = self.tri_codes_ressources() # les ressources tries
        matrice = {s: {} for s in saes}
        for s in matrice:
            matrice[s] = {r: None for r in ressources}

        # Les ressources sur lesquelles s'appuient les sae
        for s in saes:
            a = self.activites[s] # la sae
            for r in a.yaml["ressources"]:
                matrice[s][r] = -1

        # les saes qui mobilisent les ressources
        for r in ressources:
            a = self.activites[r]
            for s in a.yaml["sae"]:
                if s in matrice:
                    if not matrice[s][r]:
                        matrice[s][r] = 1
                    else:
                        matrice[s][r] += 1
                else:
                    self.__LOGGER.error(f"Dans {self.nom_semestre}, {s} (venant de {r}) n'existe pas dans les sae du semestre")
        return matrice

    def init_matrice_ac(self):
        """Initialise la matrice des acs"""
        matrice = {comp: {} for comp in self.comp_et_acs}  # les comp et les acs du semestre
        for comp in self.comp_et_acs:
            for acs in self.comp_et_acs[comp]:
                matrice[comp][acs] = {}
                for a in self.activites:
                    matrice[comp][acs][a] = False
        return matrice

    def get_matrice_ac(self):
        """Renvoie la matrice d'AC (trié par compétence) vs SAE et ressources sous la forme d'un dictionnaire :
        { comp : {acs: { activite: }}
        """
        # Initialisation de la matrice
        matrice = self.init_matrice_ac()

        for (i, code) in enumerate(self.activites):  # pour chaque activité (saé & ressource)
            a = self.activites[code]
            for comp in self.comp_et_acs:
                if comp in a.acs: # si la comp fait partie des acs de l'activité
                    for ac in a.acs[comp]:  # pour les acs de l'activité dans la comp
                        if ac in matrice[comp]:
                            matrice[comp][ac][code] = True
                        else:
                            self.__LOGGER.error(f"{ac} n'existe pas dans sem {self.nom_semestre}")
        return matrice


    def get_matrice_ac_optionnels(self):
        """Renvoie la matrice d'AC (trié par compétence) vs SAE et ressources sous la forme d'un dictionnaire :
        { comp : {acs: { activite: }}
        """
        # Initialisation de la matrice
        matrice = self.init_matrice_ac()

        for (i, code) in enumerate(self.activites):  # pour chaque activité (saé & ressource)
            a = self.activites[code]
            for comp in self.comp_et_acs:
                if comp in a.acs_optionnels: # si la comp fait partie des acs de l'activité
                    for ac in a.acs_optionnels[comp]:  # pour les acs de l'activité dans la comp
                        if ac in matrice[comp]:
                            matrice[comp][ac][code] = True
                        else:
                            self.__LOGGER.error(f"{ac} (optionnel) n'existe pas dans sem {self.nom_semestre}")
        return matrice


    def get_matrice_volumes_horaires_par_publication(self, publication):
        """Renvoie la matrice des volumes horaires par SAE et ressources sous la forme d'un dictionnaire :
        { activite : {"encadrees": ??, "cm/td": ??, "tp": ??, "projet": ??}}
        """
        # Initialisation de la matrice
        matrice = {a: {"encadrees": None,
                       "cm/td": None,
                       "cm": None,
                       "td": None,
                       "tp": None,
                       "projet": None} for a in self.activites}

        for (i, code_orebut) in enumerate(self.activites):  # pour chaque activité (saé & ressource)
            a = self.activites[code_orebut]
            code = a.code
            for champ in matrice[code_orebut]:
                if champ in a.heures[publication] and isinstance(a.heures[publication][champ], int):
                    matrice[code_orebut][champ] = a.heures[publication][champ]
                else:
                    manquant = False
                    if "SAE" not in code:
                        if champ != "projet" and publication != "pn":
                            manquant = True
                    else: # sae
                        if champ != "cm" and champ != "td" and publication != "pn":
                            manquant = True
                    if manquant:
                        matrice[code_orebut][champ] = "???"
                        # self.__LOGGER.error(f"{self}: matrice_vol_horaire: {champ} manquant pour {a.code}/{code_orebut}")
        return matrice


    def get_matrice_volumes_horaires(self):
        """Calcule la matrice donnant les volumes horaires par activités : les volumes sont distinctés entre acd (fiches), gt-but et pn"""
        matrices = {"acd": None, # les heures dans les fiches
                    "gt-but": None, # les préco du gt
                    "pn": None} # les données publiées
        for publication in matrices:
            matrices[publication] = self.get_matrice_volumes_horaires_par_publication(publication)
        return matrices

    def get_matrices_coefficients_par_parcours(self, parcours=None):
        """Calcule la matrice donnant les coefficients par activités : les coeffs sont distingués par comp ;
        elle peut être limité à un parcours donnée"""
        matrice = {}
        for (i, code_orebut) in enumerate(self.activites):  # pour chaque activité (saé & ressource)
            a = self.activites[code_orebut]
            code = a.code
            if parcours == None:
                matrice[code] = {comp: None for comp in self.officiel.DATA_COMPETENCES if comp.startswith("RT")}
            else:
                matrice[code] = {comp: None for comp in self.officiel.DATA_COMPETENCES if comp.startswith("RT") or comp.startswith(parcours)}
            for comp in matrice[code]:
                if a.coeffs and comp in a.coeffs:
                    matrice[code][comp] = a.coeffs[comp]
        return matrice

    def prepare_inclusion_fiches(self, codes_par_parcours,
                                 parcours=None,
                                 light=False):
        """Prépare une liste de fiches à inclure sur la base de leur code"""
        chaine = ""
        if not parcours:
            codes = codes_par_parcours # tous parcours
        else:
            codes = {'Tronc commun': codes_par_parcours["Tronc commun"],
                     parcours: codes_par_parcours[parcours]} # limite au parcours d'intérêt
        for p in codes:
            if codes_par_parcours[p]: # s'il y a des fiches
                # chaine += "\\subsubsection{%s}\n\n" % (p)
                chaine += "\\phantomsection \\label{subsubsec:FichesSAE%s%s}\n" % (self.nom_semestre, p.split(" ")[0])
                chaine += "\\addtocounter{subsubsection}{1}\n"
                chaine += "\\addcontentsline{toc}{subsubsection}{%s}" % (p)
                liste = []
                for code in codes_par_parcours[p]:
                    if "S" in code:
                        type = "saes"
                    else:
                        type = "ressources"
                    a = codes_par_parcours[p][code]
                    liste.append("\\input{%s/%s/%s.tex}" % (type, self.nom_semestre, a.code.replace("É", "E")))
                if not light:
                    separateur = "\\newpage\n"
                else:
                    separateur = "\\vspace{1cm}\n"
                chaine += separateur.join(liste)
                chaine += separateur
                chaine += "\n"
        return chaine


    def prepare_inclusion_matrices_AC(self, parcours=None):
        """Prépare les directives input pour l'inclusion des matrices d'AC, avec tous les
        parcours (si parcours=None) sinon uniquement le parcours visé"""
        if parcours == None:
            a_inclure = rofficiel.officiel.PARCOURS
        else:
            a_inclure = [parcours]
        if self.nom_semestre == "S1" or self.nom_semestre == "S2":
            champs = ["\\begin{center}",
                      "\\scalebox{0.75}{\\input{synthese/%s_acs_vs_saes_ressources.tex}}" % (self.nom_semestre),
                      "\\end{center}"]
        else:
            champs = []
            for p in a_inclure:
                champs.append("\\subsubsection*{Parcours %s}" % (p))
                champs.append("\\begin{center}")
                champs.append("\\scalebox{0.75}{\\input{synthese/%s_%s_acs_vs_saes_ressources.tex}}" % (self.nom_semestre, p))
                champs.append("\\end{center}")
                champs.append("\\newpage" )

        return "\n".join(champs) + "\n"


    def prepare_inclusion_volumes_horaires(self, parcours=None):
        """Ajoute les tableaux de synthèses des volumes horaires (directives input).
        Avec tous les parcours (si parcours=None) sinon uniquement le parcours visé"""
        if parcours == None:
            a_inclure = rofficiel.officiel.PARCOURS
        else:
            a_inclure = [parcours]
        if self.nom_semestre == "S1" or self.nom_semestre == "S2":
            champs = ["\\begin{center}",
                      "\\scalebox{0.9}{\\input{synthese/%s_volumes_horaires.tex}}" % (self.nom_semestre),
                      "\\end{center}"]
        else:
            champs = []
            for p in a_inclure:
                champs.append("\\subsubsection*{Parcours %s}" % (p))
                champs.append("\\begin{center}")
                champs.append("\\scalebox{0.9}{\\input{synthese/%s_%s_volumes_horaires.tex}}" % (self.nom_semestre, p))
                champs.append("\\end{center}")
                champs.append("\\newpage" )

        return "\n".join(champs) + "\n"


    def prepare_inclusion_coeffs(self, parcours=None):
        """Ajoute les tableaux de coefficients.
        Avec tous les parcours (si parcours=None) sinon uniquement le parcours visé"""
        if parcours == None:
            a_inclure = rofficiel.officiel.PARCOURS
        else:
            a_inclure = [parcours]
        if self.nom_semestre == "S1" or self.nom_semestre == "S2":
            champs = ["\\begin{center}",
                      "\\scalebox{0.95}{\\input{synthese/%s_coefficients.tex}}" % (self.nom_semestre),
                      "\\end{center}"]
        else:
            champs = []
            for p in a_inclure:
                champs.append("\\subsubsection*{Parcours %s}" % (p))
                champs.append("\\begin{center}")
                champs.append("\\scalebox{0.95}{\\input{synthese/%s_%s_coefficients.tex}}" % (self.nom_semestre, p))
                champs.append("\\end{center}")
                champs.append("\\newpage" )

        return "\n".join(champs) + "\n"


    def to_latex_description_semestres(self,
                                       modele="templates/modele_semestre.tex",
                                       modele_light="templates/modele_semestre_light.tex",
                                       modele_complementaire="templates/modele_semestre_complementaire.tex",
                                       parcours=None,
                                       light=False):
        """Génère le code latex décrivant un semestre (ses SAE, ses ressources, ...) avec ses
        input vers les matrices et les fiches
        """
        if not self.est_complementaire():
            if not light:
                modlatex = modeles.get_modele(modele)
            else: # version light
                modlatex = modeles.get_modele(modele_light)
        else: # complémentaires
            modlatex = modeles.get_modele(modele_complementaire)

        latex_inclusion_fiches_saes = self.prepare_inclusion_fiches(self.saes_par_parcours,
                                                                    parcours=parcours,
                                                                    light=light)
        latex_inclusion_fiches_ressources = self.prepare_inclusion_fiches(self.ressources_par_parcours,
                                                                          parcours=parcours,
                                                                          light=light)

        latex_matrices = self.prepare_inclusion_matrices_AC(parcours=parcours)
        latex_volumes_horaires = self.prepare_inclusion_volumes_horaires(parcours=parcours)
        latex_coeffs = self.prepare_inclusion_coeffs(parcours=parcours)

        # Prépare l'année
        if self.annee[-1] == "1":
            annee = "1ère année"
        elif self.annee[-1] == "2":
            annee = "2ème année"
        else:
            annee = "3ème année"

        # Injection dans le template
        if not self.est_complementaire():
                chaine = modeles.TemplateLatex(modlatex).substitute(
                    annee=annee,
                    numero=self.numero_semestre,
                    fichierListeSAEs="liste_saes_%s.tex" % (self.nom_semestre),
                    fichierListeRessources="liste_ressources_%s.tex" % (self.nom_semestre),
                    fichierDependance="%s_dependances_saes_vs_ressources.tex" % (self.nom_semestre),
                    # fichierMatriceACs="%s_acs_vs_saes_ressources.tex" % (self.nom_semestre),
                    fichiersMatriceACs=latex_matrices,
                    fichiersVolumesHoraires=latex_volumes_horaires,
                    fichiersCoeffs=latex_coeffs,
                    inclusion_fiches_saes=latex_inclusion_fiches_saes,
                    inclusion_fiches_ressources=latex_inclusion_fiches_ressources,
                )
        else:
            chaine = modeles.TemplateLatex(modlatex).substitute(
                        fichierListeRessources="liste_ressources_%s.tex" % (self.nom_semestre),
                        inclusion_fiches_ressources=latex_inclusion_fiches_ressources,
                    )
        return chaine


    def to_latex_matrice_ac_vs_activites(self, parcours=None):
        """Renvoie le tableau latex affichant la matrice des apprentissages critiques
        ayant connaissances des ``saes`` et des ``ressources``
        du semestre
        """
        # Les activités du parcours
        saes_du_parcours = self.tri_codes_saes(parcours=parcours)
        ressources_du_parcours = self.tri_codes_ressources(parcours=parcours)
        codes_activites = saes_du_parcours + ressources_du_parcours

        matrice = self.get_matrice_ac()
        matrice_optionnels = self.get_matrice_ac_optionnels()

        comp_et_acs_du_parcours = self.tri_comp_et_acs(parcours=parcours) # dico : comp => acs

        nbre_colonnes = len(codes_activites) + 2
        longueur = 5
        chaine = (
            "\\begin{tabular}[c]{|lp{%scm}|" % str(longueur)
            + "c|" * (len(codes_activites))
            + "}"
            + "\n"
        )
        chaine += "\\hline \n"  # % (nbre_saes + nbre_ressources+1)+ "\n"
        if parcours == None:
            nom = "Tronc Commun"
        else:
            nom = "parcours " + parcours
        chaine += "\multicolumn{%d}{|l|}{\\cellcolor{gray!25} \\large \\bfseries %s} \\\\ \n" % (nbre_colonnes, "Matrice des apprentissages critiques du %s au %s" % (nom, self.nom_semestre))
        chaine += "\\hline \n"

        # l'entete
        chaine += " & & "
        chaine += (
            "\multicolumn{%d}{c|}{\\textcolor{saeC}{\\bfseries SAÉs}}" % (len(saes_du_parcours)) + "\n"
        )
        chaine += " & "
        chaine += (
            "\multicolumn{%d}{c|}{\\textcolor{ressourceC}{\\bfseries Ressources}}"
            % (len(ressources_du_parcours))
            + "\\\\ \n"
        )
        chaine += "\\cline{3-%d}" % (nbre_colonnes)
        chaine += " & & "

        # les noms des SAE et des ressources
        noms_saes = []
        noms_ressources = []
        for (i, code) in enumerate(saes_du_parcours + ressources_du_parcours):  # pour chaque SAE
            a = self.activites[code]
            nom = rpn.latex.get_nom_affichage_intitule(a.nom, limite=40)
            contenu = "\\small{%s}" % (nom.replace("&", "\&"))
            rotation = rpn.latex.rotation_entete_colonne(contenu) + "\n"
            if code in saes_du_parcours:
                noms_saes.append(rotation)
            else:
                noms_ressources.append(rotation)
        chaine += " & ".join(noms_saes) + "\n"
        chaine += " & "
        chaine += " & ".join(noms_ressources) + "\n"
        chaine += "\\\\ \n"

        # les codes des SAE et des ressources
        noms_saes = []
        noms_ressources = []
        chaine += " & & \n"
        for (i, code) in enumerate(saes_du_parcours + ressources_du_parcours):  # pour chaque SAE
            a = self.activites[code]
            if code in saes_du_parcours:
                couleur = "saeC"
            else:
                couleur = "ressourceC"
            code_orebut = a.codeOReBUT
            contenu = "~\\hyperlink{%s}{\\textcolor{%s}{%s}}" % (a.get_code_latex_hyperlink(a.code),
                                                                 couleur,
                                                                 code_orebut) # a.code)
            rotation = rpn.latex.rotation_entete_colonne(contenu, pos="r") + "\n"
            if code in saes_du_parcours:
                noms_saes.append(rotation)
            else:
                noms_ressources.append(rotation)
        chaine += " & ".join(noms_saes) + "\n"
        chaine += " & "
        chaine += " & ".join(noms_ressources) + "\n"
        chaine += "\\\\ \n"
        chaine += "\\hline \n"

        # Les ACS et les croix self.acs
        for (noc, comp) in enumerate(comp_et_acs_du_parcours): # les comp & les acs du semestre
            nom_comp = self.officiel.DATA_COMPETENCES_DETAILLEES[comp]["nom"]
            numero_niveau = self.get_niveau_from_comp(comp) # le numero du niveau de 1 à 3
            niveau = list(self.officiel.DATA_COMPETENCES_DETAILLEES[comp]["niveaux"].keys())[numero_niveau-1]

            couleur = rpn.latex.get_couleur_comp(comp)
            chaine += (
                    "\\multicolumn{%d}{|l|}{\hyperlink{%s}{\\textcolor{%s}{\\bfseries %s - %s }}} \\\\ \n"
                    % (nbre_colonnes, comp, couleur, comp,
                       nom_comp.replace("&", "\&"))
                )
            chaine += "\\multicolumn{%d}{|l|}{\small Niveau %d - %s} \\\\ \n" % (
                    nbre_colonnes,
                    numero_niveau,
                    niveau.replace("&", "\&"),
                )
            chaine += "\\hline \n"
            for ac in comp_et_acs_du_parcours[comp]:
                chaine += "\\textcolor{%s}{%s} & \n" % (couleur, ac)
                chaine += "\\begin{tabular}{p{%scm}} " % (str(longueur - 0.2))
                nom_ac = self.comp_et_acs[comp][ac].replace("&", "\&")
                chaine += "\\tiny{\\textit{" + nom_ac + "}}"
                chaine += "\\end{tabular} & \n"

                croix_saes = []
                croix_ressources = []
                for (i, code) in enumerate(saes_du_parcours + ressources_du_parcours):  # pour chaque SAE
                    a = self.activites[code]
                    if matrice[comp][ac][code] == True:
                        valeur = "$\\times$"
                    elif matrice_optionnels[comp][ac][code] == True:
                        valeur = "$\\sim$"
                    else:
                        valeur = ""
                    if code in saes_du_parcours:
                        croix_saes.append(valeur)
                    else:
                        croix_ressources.append(valeur)

                chaine += " & ".join(croix_saes) + "\n"
                chaine += " & "
                chaine += " & ".join(croix_ressources) + "\\\\ \n"
                chaine += "\\hline \n"
            if noc < len(comp_et_acs_du_parcours) - 1: # si pas la dernière comp
                chaine += "\\hline \n"

        chaine += "\\end{tabular}"

        return chaine


    def to_latex_matrice_dependance(self):
        """Renvoie le tableau latex affichant la matrice des dépendances SAEs, ressources
        """

        # Les activités du parcours
        saes = self.tri_codes_saes()
        ressources = self.tri_codes_ressources()

        matrice = self.get_matrices_dependances()

        longueur = 8
        chaine = (
                "\\begin{tabular}[c]{|rl|" # % str(longueur)
                + "c|" * (len(ressources))
                + "}"
                + "\n"
        )
        chaine += "\\hline \n"  # % (nbre_saes + nbre_ressources+1)+ "\n"
        chaine += "\\multicolumn{%d}{|l|}{\\large \cellcolor{gray!25} \\bfseries %s} \\\\ \n" % (len(ressources)+2, "Dépendances SAEs vs Ressources au sein du %s" % (self.nom_semestre))
        chaine += "\\hline \n"
        # l'entete
        chaine += " & & "
        # les noms des SAE et des ressources
        noms_ressources = []
        for (i, code) in enumerate(ressources):  # pour chaque SAE
            a = self.activites[code]
            nom = rpn.latex.get_nom_affichage_intitule(a.nom.replace("&", "\&"), limite=50) # le nom à afficher, limité en caractère
            contenu = "\\small{%s}" % (nom)
            rotation = rpn.latex.rotation_entete_colonne(contenu) + "\n"
            noms_ressources.append(rotation)
        chaine += " & ".join(noms_ressources) + "\n"
        chaine += "\\\\ \n"

        # les codes des SAE et des ressources
        noms_ressources = []
        chaine += " & & \n"
        for (i, code) in enumerate(ressources):  # pour chaque ressource
            a = self.activites[code]
            couleur = "ressourceC"
            code_orebut = a.codeOReBUT
            contenu = "~\\hyperlink{%s}{\\textcolor{%s}{%s}}" % (a.get_code_latex_hyperlink(a.code),
                                                                 couleur,
                                                                 code_orebut) #a.code)
            rotation = rpn.latex.rotation_entete_colonne(contenu, pos="r") + "\n"
            noms_ressources.append(rotation)
        chaine += " & ".join(noms_ressources) + "\n"
        chaine += "\\\\ \n"
        chaine += "\\hline \n"

        # Les dépendances
        for s in saes:  # les comp & les acs du semestre
            asae = self.activites[s]

            couleur = "saeC"
            code_orebut = asae.codeOReBUT
            chaine += "~\\hyperlink{%s}{\\textcolor{%s}{%s}} " % (asae.get_code_latex_hyperlink(asae.code),
                                                                 couleur,
                                                                 code_orebut) # asae.code)
            chaine += " & "
            # chaine += "\\begin{tabular}{p{%scm}} " % (str(longueur - 0.2))
            nom_sae = rpn.latex.get_nom_affichage_intitule(asae.nom.replace("&", "\&"), limite=50-len(code_orebut))
            chaine += "\\small{" + nom_sae + "}"
            # chaine += "\\end{tabular} "
            chaine += " & \n"

            croix_saes = []
            for r in ressources:  # pour chaque SAE
                aressource = self.activites[r]
                valeur = ""
                if matrice[s][r] == -1:
                    valeur = "$\\uparrow$"
                elif matrice[s][r] == 1:
                    valeur = "$\\leftarrow$"
                elif matrice[s][r] == 0:
                    valeur = "$\\times$"
                croix_saes.append(valeur)
            chaine += " & ".join(croix_saes) + "\n"
            chaine += "\\\\ \n \\hline \n"

        chaine += "\\end{tabular}"
        return chaine


    def to_latex_matrice_volumes_horaires_par_parcours(self,
                                                       parcours=None,
                                                       publications = ["pn", "gt-but"], #, "acd"],
                                                       light=False):
        """Renvoie le tableau latex affichant la matrice des coeffs et des volumes horaires
        par parcours.
        Si parcours=None, tous les parcours sont indiqués. Sinon, seul le parcours mentionné
        est considéré
        Si light=True, seuls les volumes du gt-but (tableur pacd) sont mentionnés."""

        # Les activités du parcours
        saes_du_parcours = self.tri_codes_saes(parcours=parcours)
        ressources_du_parcours = self.tri_codes_ressources(parcours=parcours)
        codes_activites = saes_du_parcours + ressources_du_parcours

        matrice_vols = self.get_matrice_volumes_horaires()

        # réduit les publi dans le cas d'une version light
        if light:
            publications = ['gt-but']

        def str_volume(val):
            return str(val) if isinstance(val, int) or isinstance(val, str) else " " # (str(val) + "h")

        # créé les colonnes
        colonnes = {p: [] for p in publications}
        for pub in colonnes:
            if pub == "pn":
                colonnes[pub] = ["encadrees", "cm/td", "tp", "projet"]
            elif pub == "gt-but":
                colonnes[pub] = ["encadrees", "cm/td", "cm", "td", "tp", "projet"]
            else: # acd
                colonnes[pub] = ["cm", "td"] # ["encadrees", "cm/td", "cm", "td", "tp", "projet"]

        nbre_colonnes = 10 # sum([len(colonnes[p]) for p in colonnes]) +2
        longueur = 5.5
        chaine = (
                "\\begin{tabular}[c]{|rl|" # p{%scm}|" % str(longueur)
                + "c|" * (nbre_colonnes)
                + "}"
                + "\n"
        )
        chaine += "\\hline \n"
        if parcours == None:
            nom = "Tronc Commun"
        else:
            nom = "parcours " + parcours
        affichage = "Volumes horaires du %s au %s" % (nom, self.nom_semestre)
        chaine += "\multicolumn{%d}{|l|}{\\cellcolor{gray!25} \\large \\bfseries %s} \\\\ \n" % (nbre_colonnes+2, affichage)
        chaine += "\\hline \n"
        # chaine += "\n"
        # l'en-tête/début : 1ère ligne : la provenance des heures
        chaine += " & & "
        col_latex = []
        INTITULES = {"pn": ["Publié", "au PN"],
                     "gt-but": ["Préconisé", "par le GT/ACD"],
                     "acd": ["Indiqué", "dans les fiches"]}
        for p in publications:
            col_latex += ["\\multicolumn{%d}{c|}{\\bfseries %s}\n" % (len(colonnes[p]), INTITULES[p][0])]
        chaine += " & ".join(col_latex)
        chaine += " \\\\ \n"
        chaine += " & & "
        col_latex = []
        for p in publications:
            col_latex += ["\\multicolumn{%d}{c|}{\\bfseries %s}\n" % (len(colonnes[p]), INTITULES[p][1])]
        chaine += " & ".join(col_latex)
        chaine += " \\\\ \n"
        # l'entete suite : le descriptif des volumes
        chaine += "\\cline{3-%d}" % (nbre_colonnes+2)
        chaine += " & & "
        # Volume
        col2_latex = []
        for p in publications:
            for c in colonnes[p]:
                nom = rpn.latex.get_nom_affichage_heures(c)
                if c == "cm" or c == "td":
                    col2_latex += [rpn.latex.rotation_entete_colonne("\\textit{%s}" % (nom))]
                else:
                    col2_latex += [rpn.latex.rotation_entete_colonne("\\bfseries " + nom + "~~~")]
        chaine += " & ".join(col2_latex)
        chaine += "\\\\ \n"
        chaine += "\\hline "

        # Les SAES
        # le nom des SAE : saes_du_parcours
        for (i, code) in enumerate(codes_activites):  # pour chaque SAE
            a = self.activites[code]
            code_orebut = a.codeOReBUT
            if code in saes_du_parcours:
                couleur = "saeC"
            else:
                couleur = "ressourceC"
            hyper_link = a.get_code_latex_hyperlink(a.code)
            chaine += "~\\hyperlink{%s}{\\textcolor{%s}{%s}}" % (hyper_link, couleur, code_orebut                                                                     )
            chaine += " & " + "\n"
             # TODO: à effacer dans le PN final a.codeRT
            nom = rpn.latex.get_nom_affichage_intitule(a.nom, limite=50)
            nom = nom.replace("&", "\&")
            # chaine += "\\begin{tabular}{p{5.5cm}} "
            chaine += "\\small{%s}" % (nom) # (a.codeRT + " : " + nom)
            # chaine += "\\end{tabular}"
            chaine += "& \n"

            # les volumes horaires
            vol_latex = []
            for p in publications:
                for c in colonnes[p]:
                    vol = matrice_vols[p][code][c] # le volume écrit
                    if p == "gt-but" and c in ["cm", "td"]:
                        vol = matrice_vols["acd"][code][c] # le vol préconisé par l'acd
                        if not isinstance(vol, int): # valeur manquante
                            vol_latex += [""]
                        else:
                            vol_latex += ["\\textit{\\small %s}" % (str_volume(vol))]
                    elif p == "acd" and c in ["cm", "td"] and not isinstance(vol, int): # valeur manquante
                        vol_latex += [""]
                    else:
                        if c == "cm" or c == "td":
                            vol_latex += ["\\textit{\\small %s}" % (str_volume(vol))]
                        elif c == "encadrees":
                            vol_latex += ["\\textcolor{%s}{%s}" % (couleur, str_volume(vol))]
                        else:
                            vol_latex += [str_volume(vol)]
            chaine += " & ".join(vol_latex)
            chaine += "\\\\ \n"
            chaine += "\\hline "

        # total des heures
        total_ressources = self.get_total_nbre_heures_par_modalite(parcours=parcours,
                                                                   type="ressources",
                                                                   publications=publications)
        total_saes = self.get_total_nbre_heures_par_modalite(parcours=parcours,
                                                             type="saes",
                                                             publications=publications)
        total = self.get_total_nbre_heures_par_modalite(parcours=parcours,
                                                        type=None,
                                                        publications=publications)
        chaine += "\\hline"
        for (i, tot) in enumerate([total_saes, total_ressources, total]):
            if i == 0:
                entete = "Total sur les SAEs"
            elif i == 1:
                entete = "Total sur les ressources"
            else:
                entete = "Total sur les SAEs et les ressources"
            chaine += "\\multicolumn{2}{|r|}{%s} " % (entete)
            chaine += " & \n "
            vol_latex = []
            for p in publications:
                for c in colonnes[p]:
                    vol = 0
                    if c not in ["cm", "td"]:
                        vol = tot[p][c] # les cm/td de l'acd
                    if c in ["cm", "td"]:  # valeur non significative
                        vol_latex += [""]
                    elif vol == 0:
                        vol_latex += [""] # valeur non significative
                    elif c == "encadrees":
                        vol_latex += ["\\textcolor{gray}{\\textbf{%s}}" % (str_volume(vol))]
                    else:
                        vol_latex += ["\\textbf{%s}" % (str_volume(vol))]
            chaine += " & ".join(vol_latex)
            chaine += " \\\\ \n"
            chaine += "\\hline "

        # chaine += "\\hline "

        # ECTS
        # chaine += r"""\multicolumn{5}{l}{~}\\
    # \multicolumn{5}{l}{\bfseries Crédits ECTS}\\
    # \hline
    # \multicolumn{5}{|l|}{} & RT1 & RT2 & \multicolumn{1}{c|}{RT3} \\
    #    \hline
    # \multicolumn{5}{|l|}{} & %d & %d & %d \\
    #    \hline
    #    """ % tuple(Config.ECTS[self.numero_semestre][ue] for ue in Config.ECTS[self.numero_semestre])
        chaine += "\\end{tabular}"
        return chaine


    def to_latex_matrice_coefficients_par_parcours(self,
                                                       parcours=None,
                                                       light=False):
        """Renvoie le tableau latex affichant la matrice des coeffs et des volumes horaires
        par parcours.
        Si parcours=None, tous les parcours sont indiqués. Sinon, seul le parcours mentionné
        est considéré
        Si light=True, seuls les volumes du gt-but (tableur pacd) sont mentionnés."""

        # Les activités du parcours
        saes_du_parcours = self.tri_codes_saes(parcours=parcours)
        ressources_du_parcours = self.tri_codes_ressources(parcours=parcours)
        codes_activites = saes_du_parcours + ressources_du_parcours
        comp = self.tri_comp_et_acs(parcours=parcours)

        matrices_coeffs = self.get_matrices_coefficients_par_parcours(parcours=parcours)


        def str_coeff(val):
            return str(val) if isinstance(val, int) or isinstance(val, str) else " " # (str(val) + "h")

        # créé les colonnes
        colonnes = {c: [] for c in comp}

        nbre_colonnes = len(colonnes) + 1
        longueur = 7.5
        chaine = (
                "\\begin{tabular}[c]{|rl|" # % str(longueur)
                + "c|" * (nbre_colonnes - 1)
                + "c|" # colonne de total
                + "}"
                + "\n"
        )
        chaine += "\\hline \n"
        # l'en-tête/début : 1ère ligne : la provenance des heures
        if parcours == None:
            nom = "Tronc Commun"
        else:
            nom = "parcours " + parcours
        affichage =  "Coefficients du %s au %s" % (nom, self.nom_semestre)
        chaine += "\multicolumn{%d}{|l|}{\\cellcolor{gray!25} \\large \\bfseries %s} \\\\ \n" % (nbre_colonnes+2, affichage)
        chaine += "\\hline \n"
        chaine += " & & "
        col_latex = []
        for c in comp:
            nom = self.officiel.get_diminutif_comp_from_code_comp(c)
            couleur = rpn.latex.get_couleur_comp(c)
            nom_comp = "\\textcolor{%s}{%s - %s}~~~~" % (couleur, c, nom)
            rotation = rpn.latex.rotation_entete_colonne(nom_comp) + "\n"
            col_latex += [rotation]
        chaine += " & ".join(col_latex)
        # Ajout de la col total
        rotation = rpn.latex.rotation_entete_colonne("Total toutes compétences") + "\n"
        chaine += " & " + rotation
        chaine += "\\\\ \n"
        chaine += "\\hline "

        # Les SAES
        # le nom des SAE : saes_du_parcours
        for (i, code) in enumerate(codes_activites):  # pour chaque SAE
            a = self.activites[code]
            code_orebut = a.codeOReBUT
            if code in saes_du_parcours:
                couleur = "saeC"
            else:
                couleur = "ressourceC"
            hyper_link = a.get_code_latex_hyperlink(a.code)
            chaine += "~\\hyperlink{%s}{\\textcolor{%s}{%s}}" % (hyper_link, couleur, code_orebut)
            chaine += " & " + "\n"

            nom = rpn.latex.get_nom_affichage_intitule(a.nom, limite=50)
            nom = nom.replace("&", "\&")
            chaine += " \\small{%s} & \n" % (nom)

            # les coeff horaires
            coeff_latex = []
            for c in comp:
                coeff_latex += [str_coeff(matrices_coeffs[code][c])]

            chaine += " & ".join(coeff_latex)
            chaine += " & " # la col de total
            chaine += "\\\\ \n"
            chaine += "\\hline "

        # total des heures
        total_coeff = {}
        total = {} # total toute comp
        for type in ["saes", "ressources", "tout"]:
            total_coeff[type] = self.get_total_coeffs_par_parcours(parcours=parcours, type=type)
            total[type] = sum(total_coeff[type].values())

        chaine += "\\hline "
        for type in ["saes", "ressources", "tout"]:
            if type == "saes":
                entete = "Total sur les SAEs"
            elif type == "ressources":
                entete = "Total sur les ressources"
            else:
                entete = "Total sur les SAEs et les ressources"
            chaine += "\\multicolumn{2}{|r|}{%s} " % (entete)
            chaine += " & \n "
            vol_latex = []
            for c in comp:
                vol = total_coeff[type][c]
                if vol == 0:
                    vol_latex += [""] # valeur non significative
                else:
                    vol_latex += ["\\textbf{%s}" % str_coeff(vol)]
            chaine += " & ".join(vol_latex)
            chaine += " & " + str_coeff(total[type])
            chaine += " \\\\ \n"
            chaine += "\\hline "

        # chaine += "\\hline "

        # ECTS
        # chaine += r"""\multicolumn{5}{l}{~}\\
    # \multicolumn{5}{l}{\bfseries Crédits ECTS}\\
    # \hline
    # \multicolumn{5}{|l|}{} & RT1 & RT2 & \multicolumn{1}{c|}{RT3} \\
    #    \hline
    # \multicolumn{5}{|l|}{} & %d & %d & %d \\
    #    \hline
    #    """ % tuple(Config.ECTS[self.numero_semestre][ue] for ue in Config.ECTS[self.numero_semestre])
        chaine += "\\end{tabular}\n\n"
        return chaine


    def get_total_nbre_heures_par_modalite(self, parcours=None,
                                           type=None,
                                           publications=["pn", "gt-but", "acd"]):
        """Calcule le nombre d'heures total d'après la matrice des volumes horaires,
        sur les 3 modalités (CM/TD, TP, projet).
        Par défaut, saés et ressources sont pris en compte.

        `type` permet de limiter le calcul soit aux `"saés"`, soit aux `"ressources"`.
        """
        saes_du_parcours = self.tri_codes_saes(parcours=parcours)
        ressources_du_parcours = self.tri_codes_ressources(parcours=parcours)
        if type == "saes":
            codes_activites = saes_du_parcours
        elif type == "ressources":
            codes_activites = ressources_du_parcours
        else:
            codes_activites = saes_du_parcours + ressources_du_parcours

        matrice_heures = self.get_matrice_volumes_horaires()

        total = {p: {} for p in publications}
        for pub in publications:
            if pub == "pn" or pub == "gt-but":
                total[pub] = {"encadrees": 0, "cm/td": 0, "tp": 0, "projet": 0}
            else:
                total[pub] = {"encadrees": 0, "cm/td": 0, "cm": 0, "td": 0, "tp": 0, "projet": 0}

        for pub in publications:
            for champ in total[pub]:
                for code_orebut in codes_activites:
                    if champ in matrice_heures[pub][code_orebut]:
                        if isinstance(matrice_heures[pub][code_orebut][champ], int):
                            total[pub][champ] += matrice_heures[pub][code_orebut][champ]

        return total


    def get_total_coeffs_par_parcours(self, parcours=None, type=""):
        """Calcule le total des coeffs pour toutes les comp d'un parcours. parcours peut valoir TC pour le S1 et le S2
        `type` permet de limiter le calcul soit aux `"saés"`, soit aux `"ressources"`.
        """
        total = {}
        comp_et_acs_du_parcours = self.tri_comp_et_acs(parcours=parcours) # dico : comp => acs
        for comp in comp_et_acs_du_parcours: # dico : comp => acs: # les comps du semestre et du parcours
            total[comp] = self.get_total_coeffs_par_comp_et_par_parcours(comp, parcours=parcours, type=type)
        return total


    def get_total_coeffs_par_comp_et_par_parcours(self, comp, parcours=None, type=""):
        """Calcule le total (somme) des coeffs par compétence et parcours, en utilisant la matrice
        des coefficients.
        Par défaut, saés et ressources sont pris en compte.

        `type` permet de limiter le calcul soit aux `"saés"`, soit aux `"ressources"`.
        """
        if parcours == None:
            matrice_coeffs = self.matrices_coefficients["TC"]
        else:
            matrice_coeffs = self.matrices_coefficients[parcours]
        saes_du_parcours = self.tri_codes_saes(parcours=parcours)
        ressources_du_parcours = self.tri_codes_ressources(parcours=parcours)
        if type == "saes":
            activites = saes_du_parcours
        elif type == "ressources":
            activites = ressources_du_parcours
        else:
            activites = saes_du_parcours + ressources_du_parcours
        total = 0
        for code in activites:
            a = self.activites[code]
            cof = matrice_coeffs[code][comp]
            if isinstance(cof, int):
                total += cof
        return total



    def affiche_bilan_heures(self):
        """Renvoie une chaine décrivant un bilan des heures-ressources du semestre

        .. deprecated:: à revoir
        """
        ligne = "{:20s} | {:75s} | {:10s} | {:10s} |"
        trait = "-"*len(ligne.format("", "", "", ""))

        ressem = self.ressources # les ressources du semestre
        chaine = ""
        chaine += trait + "\n"
        chaine += ligne.format("Code", "Ressource", "Form.", "dont TP") + "\n"
        chaine += trait + "\n"
        for (code, r) in self.ressources.items():
            chaine += ligne.format(r.code if r.code else "MANQUANT",
                                   # r.nom[:30] + ("..." if len(r.nom) > 30 else "") ,
                                   r.nom,
                                   str(r.heures_formation) if r.heures_formation else "MANQUANT",
                                   str(r.tp) if r.tp else "MANQUANT") + "\n"
        heures_formation_total = sum([r.heures_formation for r in ressem if r.heures_formation != None])
        heures_tp_total = sum([r.tp for r in ressem if r.tp != None])
        chaine += trait + "\n"
        chaine += ligne.format("", "Total", str(heures_formation_total), str(heures_tp_total)) + "\n"
        chaine += trait + "\n"
        return chaine


    def get_matrice_prerequis(self):
        """Renvoie la matrice traduisant les dépendances entre les SAés et les
        ressources d'un même semestre (les activités hors semestres ne sont pas considérées).

        Une activité i dépend d'une activité j si l'activité j fait partie des pré-requis de l'activité i.
        """
        matrice = [[0] * (self.nbre_saes + self.nbre_ressources) for i in range(len(self.codes))]

        for (i, code) in enumerate(self.codes):  # pour chaque SAE ou ressources
            act = self.activites[code]
            prerequis = []
            if "prerequis" in act.yaml:
                prerequis = act.yaml["prerequis"]

            for (j, code_prerequis) in enumerate(self.codes):
                if code_prerequis in prerequis:
                    matrice[i][j] = 1
        return matrice



    def to_latex_liste_activites(self, activites_par_parcours,
                                 modele="templates/modele_liste_sae_par_semestre.tex",
                                 parcours=None,
                                 light=False):
        """Prépare les codes latex d'une liste d'activité triée par parcours.
        Si parcours=None, tous les parcours sont pris en compte ; sinon, seul le parcours demandé est
        affiché"""

        modlatex = modeles.get_modele(modele)

        liste_saes_et_exemples = []
        if parcours == None:
            activites_a_inclure = activites_par_parcours
        else:
            activites_a_inclure = {'Tronc commun':
                                       activites_par_parcours['Tronc commun'],
                                   parcours: activites_par_parcours[parcours]}
        for p in activites_a_inclure:
            if len(activites_par_parcours[p]) > 0:
                titre = "\\multicolumn{3}{|l|}{\\bfseries %s}" % ("Tronc commun" if "Tronc" in p else "Parcours %s" % p)
                liste_saes_et_exemples.append(titre)
            for sae in activites_par_parcours[p]: # les saes du semestre et du parcours
                a = activites_par_parcours[p][sae] # l'activite
                info_sae = []
                hyperlink = a.get_code_latex_hyperlink(a.code)
                couleur = "saeC" if a.est_sae_from_code() else "ressourceC"
                code_orebut = a.codeOReBUT
                code = "\\bfseries \\hyperlink{%s}{\\textcolor{%s}{%s}}" % (hyperlink, couleur, code_orebut)
                # if not light:
                #    titre = "%s : " % (a.codeRT)
                # else:
                titre = ""
                titre += a.nom.replace("&", "\&")
                page = "\\pageref{subsubsec:%s}" % (hyperlink)
                info_sae.append( " & ".join([code, titre, page]))

                # Inutilisé maintenant
                for exemple in []: # self.exemples[sae]:
                    code = ""
                    titre = "Exemple 1:"
                    # page = "\"
                    info_sae.append( " & ".join([code, titre, page]) )

                liste_saes_et_exemples.append("\n\\tabularnewline\n".join(info_sae))

            tableau = "\n \\tabularnewline \\hline \n".join(liste_saes_et_exemples) + "\n\\tabularnewline\n"

        chaine = modeles.TemplateLatex(modlatex).substitute(
            liste_sae_par_semestre=tableau
            # listeExemples = A FAIRE
        )
        # chaine = chaine.replace("&", "\&")
        return chaine



    def to_latex_liste_activites_et_volumes(self, activites_par_parcours,
                                 modele="templates/modele_liste_sae_et_volumes_par_semestre.tex",
                                 parcours=None,
                                 light=False):
        """Prépare les codes latex d'une liste d'activité triée par parcours.
        Si parcours=None, tous les parcours sont pris en compte ; sinon, seul le parcours demandé est
        affiché"""

        modlatex = modeles.get_modele(modele)
        volumes_horaires = self.get_matrice_volumes_horaires()

        liste_saes_et_exemples = []
        if parcours == None:
            self.__LOGGER.error("to_latex_liste_activites_et_volumes: Non prévu pour tous les parcours")
        else:
            activites_a_inclure = {'Tronc commun': activites_par_parcours['Tronc commun'],
                                   parcours: activites_par_parcours[parcours]}
        for p in activites_a_inclure:
            if len(activites_par_parcours[p]) > 0:
                titre = "\\multicolumn{3}{|l|}{\\bfseries %s}" % ("Tronc commun" if "Tronc" in p else "Parcours %s" % p)
                liste_saes_et_exemples.append(titre)
            for sae in activites_par_parcours[p]: # les saes du semestre et du parcours
                a = activites_par_parcours[p][sae] # l'activite
                info_sae = []
                hyperlink = a.get_code_latex_hyperlink(a.code)
                couleur = "saeC" if a.est_sae_from_code() else "ressourceC"
                code_orebut = a.codeOReBUT
                code = "\\bfseries \\hyperlink{%s}{\\textcolor{%s}{%s}}" % (hyperlink, couleur, code_orebut)
                if not light:
                    titre = "%s : " % (a.codeRT)
                else:
                    titre = ""
                titre += a.nom.replace("&", "\&")
                vol = volumes_horaires["gt-but"][sae]["encadrees"]
                if isinstance(vol, int):
                    formation = str(vol) + "h"
                else:
                    formation = ""
                vol = volumes_horaires["gt-but"][sae]["projet"]
                if isinstance(vol, int):
                    projet = str(vol) + "h"
                else:
                    projet = ""

                page = "\\pageref{subsubsec:%s}" % (hyperlink)
                info_sae.append( " & ".join([code, titre, formation, projet, page]))

                liste_saes_et_exemples.append("\n\\tabularnewline\n".join(info_sae))

            tableau = "\n \\tabularnewline \\hline \n".join(liste_saes_et_exemples) + "\n\\tabularnewline\n"

        chaine = modeles.TemplateLatex(modlatex).substitute(
            liste_sae_par_semestre=tableau
            # listeExemples = A FAIRE
        )
        # chaine = chaine.replace("&", "\&")
        return chaine


    def to_xml(self, ref_xml):
        """Export xml du semestre pour orebut avec injection dans ref_xml"""

        donnees = ET.SubElement(ref_xml, "semestre")
        donnees.set("numero", self.numero_semestre)
        donnees.set("libelle", self.nom_semestre)
        donnees.set("ordreAnnee", self.annee[-1])
        donnees_saes = ET.SubElement(donnees, "saes") # les saes
        for sae in self.saes: # pour les saes
            self.activites[sae].to_xml(donnees_saes)

        donnees_ressources = ET.SubElement(donnees, "ressources") # les ressources
        for res in self.ressources:
            self.activites[res].to_xml(donnees_ressources)

    def to_bdd(self, connexion):
        """Ajoute le semestre à la BDD (le no semestre et son lien avec l'année étant déjà dans la BDD)"""
        for act in self.activites:
            self.activites[act].to_bdd(connexion, self.numero_semestre)

