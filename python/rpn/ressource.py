import logging

import rpn.latex
from config import Config
import rpn.activite
import modeles, string
import rofficiel.officiel
import lxml.etree as ET

class Ressource(rpn.activite.ActivitePedagogique):
    """Modélise une ressource."""

    __LOGGER = logging.getLogger(__name__)

    def __init__(self, fichieryaml, officiel):
        """Initialise les informations sur la ressource à partir du ``fichieryaml``
        et stocke les données ``officiel``les
        """
        super().__init__(fichieryaml, officiel)
        self.nom = self.yaml["nom"]
        self.codeOReBUT = self.yaml["codeOReBUT"]
        self.acs = self.yaml["acs"]
        self.acs_optionnels = self.yaml["acs_optionnels"]
        self.annee = self.yaml["annee"]
        self.parcours = self.yaml["parcours"]
        # self.competences = self.yaml["competences"]
        self.coeffs = self.yaml["coeffs"]
        # Les heures
        self.heures = self.yaml["heures"]
        self.ordre = self.yaml["ordre"]

        # L'adaptation locale
        self.type = self.yaml["type"]
        self.adaptation = self.yaml["type"]["adaptation"]



    def prepare_prerequis(self):
        """Prépare l'affichage des pré-requis"""
        if self.yaml["prerequis"] == rofficiel.officiel.AUCUN_PREREQUIS:
            latex_prerequis = "\\textit{Aucun}"
        else:
            # est-une liste de ressources
            if not self.yaml["prerequis"][0].startswith("R"):
                latex_prerequis = self.yaml["prerequis"]
            else:
                latex_prerequis = self.to_latex_liste_fiches(self.yaml["prerequis"])
        return latex_prerequis

    def prepare_contexte(self):
        """Prépare le contexte/contenu/objectifs/prolongements)"""
        if not self.yaml["contexte"] or self.yaml["contexte"] == "Aucun":
            contexte = "Aucun"
        else:
            contexte = self.yaml["contexte"]
        return rpn.latex.md_to_latex(contexte, self.officiel.DATA_MOTSCLES)

    def prepare_description(self):
        """Prépare les contenus"""
        champ = []

        # généralités (optionnel)
        generalites = self.yaml["généralités"]
        latex_generalites = ""
        if generalites != "Aucun":
            latex_generalites = self.to_latex_champ_titre("Généralités", generalites)


        # contexte obligatoire
        contexte = self.yaml["contexte"]
        latex_contexte = ""
        if contexte:
            latex_contexte = self.to_latex_champ_titre("Contexte et ancrage professionnel", contexte)

        contenus = self.yaml["contenus"]
        latex_contenus = ""
        if contenus:
            latex_contenus = self.to_latex_champ_titre("Contenus", contenus)
            if latex_contenus.endswith("\n"):
                latex_contenus = latex_contenus[:-1]  # supprime le dernier passage à la ligne

        # optionnel
        preco = self.yaml["préconisations"]
        latex_preco = ""
        if preco:
            latex_preco = self.to_latex_champ_titre("Préconisations de mise en oeuvre", preco)

        # optionnel
        prolongement = self.yaml["prolongements"]
        latex_prolongement = ""
        if prolongement:
            latex_prolongement = self.to_latex_champ_titre("Prolongements", prolongement)

        if latex_generalites:
            champ.append(latex_generalites)
        champ.append(latex_contexte)
        champ.append(latex_contenus)
        if latex_preco:
            champ.append(latex_preco)
        if latex_prolongement:
            champ.append(latex_prolongement)
        return "\n\n\\vspace{0.2cm}\n".join(champ)

    def to_xml_prepare_description(self):
        """Prépare les contenus"""
        champ = []

        # généralités optionel
        generatites = self.yaml["généralités"]
        latex_generalites = ""
        if generatites != "Aucun":
            latex_contexte = "**Généralités** : \n"
            latex_contexte += generatites

        # contexte obligatoire
        contexte = self.yaml["contexte"]
        latex_contexte = ""
        if contexte:
            latex_contexte = "**Contexte et ancrage professionnel** : \n\n"
            latex_contexte += contexte

        contenus = self.yaml["contenus"]
        latex_contenus = ""
        if contenus:
            latex_contenus = "**Contenus** : \n\n"
            latex_contenus += contenus
            # if latex_contenus.endswith("\n"):
            #    latex_contenus = latex_contenus[:-1]  # supprime le dernier passage à la ligne

        # optionnel
        prolongement = self.yaml["prolongements"]
        latex_prolongement = ""
        if prolongement:
            latex_prolongement = "**Prolongements** : \n\n"
            latex_prolongement += prolongement

        if latex_generalites:
            champ.append(generatites)
        champ.append(latex_contexte)
        champ.append(latex_contenus)
        if latex_prolongement:
            champ.append(latex_prolongement)
        return "\n\n".join(champ)

    def prepare_exemple(self):
        """Prépare l'exemple de mise en oeuvre"""
        if self.yaml["exemple"]:
            latex_exemple = "\\begin{tabular}{|G|}\n"
            latex_exemple += "\\hline\n"
            latex_exemple += "\\textcolor{ressourceC}{\\bfseries Exemple de mise en \\oe{}uvre} \\\\\n"
            latex_exemple += "\\hline\n"
            latex_exemple += rpn.latex.md_to_latex(self.yaml["exemple"], self.officiel.DATA_MOTSCLES)
            latex_exemple += "\\\\\n\\hline\n"
            latex_exemple += "\\end{tabular}\n"
        else:
            latex_exemple = ""
        return latex_exemple


    def to_latex(self, modele="templates/modele_tableau_ressource.tex",
                 modele_light="templates/modele_tableau_ressource_light.tex",
                 light=False):
        """Génère le code latex décrivant la fiche de la ressource avec une mise en forme
        sous forme de tableau, en utilisant le template latex donné dans ``modele``.
        """
        if not light:
            modlatex = modeles.get_modele(modele)
        else:
            modlatex = modeles.get_modele(modele_light)

        # Préparation des compétences, des ACs et des coeffs \ajoutRcomp + \ajoutRcoeff + boucle \ajoutRacs
        latex_competences = self.to_latex_competences_et_acs()
        # Les optionnels
        if self.acs_optionnels:
            latex_acs_optionnels = self.to_latex_acs_optionnels()
        else:
            latex_acs_optionnels = ""
        # Préparation des sae
        latex_sae = self.to_latex_liste_fiches(self.yaml["sae"])

        # Préparation des prérequis
        latex_prerequis = self.prepare_prerequis()

        # préparation de la description
        latex_description = self.prepare_description()

        # Prépare les parcours
        latex_parcours = self.yaml["parcours"]

        # Prépare les infos sur le cursus
        latex_cursus = self.prepare_cursus()

        # Prépare l'exemple
        latex_exemple = self.prepare_exemple()

        # Prépare les heures de formation
        latex_heures_acd = self.prepare_heures("acd")
        latex_heures_gt_but = self.prepare_heures("gt-but")
        latex_heures_pn = self.prepare_heures("pn")

        # Prépare les coefficients
        latex_coeff = self.to_latex_prepare_coeffs()

        # mots clés
        mots = self.yaml["motscles"]
        if mots[-1] != ".":
            mots += "."
        # Injection dans le template
        chaine = modeles.TemplateLatex(modlatex).substitute(
            codelatex=self.get_code_latex_hyperlink(self.code),
            code=self.codeOReBUT, # self.code,
            codeRT=self.codeRT,
            url=rpn.latex.md_to_latex(self.yaml["url"] if "url" in self.yaml else "", self.officiel.DATA_MOTSCLES),
            nom=self.prepare_nom(),
            cursus= latex_cursus,
            heures_acd=latex_heures_acd,
            heures_pn= latex_heures_pn,
            heures_gt_but= latex_heures_gt_but,
            parcours=latex_parcours,
            description=latex_description,
            exemple=rpn.latex.nettoie_latex(latex_exemple,
                                             self.officiel.DATA_ABBREVIATIONS),
            competences_et_acs=latex_competences, # les compétences
            acs_optionnels = latex_acs_optionnels,
            coefficients=latex_coeff,
            listeSAE=latex_sae,
            listePreRequis=latex_prerequis,
            motsCles=rpn.latex.nettoie_latex(mots,
                                             self.officiel.DATA_ABBREVIATIONS) if self.yaml["motscles"] else ""
        )
        # contenu=rpn.latex.nettoie_latex(latex_contenu, self.officiel.DATA_ABBREVIATIONS),
        # chaine = chaine.replace("&", "\&")
        # chaine = rpn.latex.nettoie_latex(chaine, self.officiel.DATA_ABBREVIATIONS)

        # Insère les abbréviations
        return chaine

    def to_xml(self, donnees_xml):
        """Intègre l'export xml de la ressource dans donnees_ressources"""
        print(f"Export de {self.codeRT}/{self.code}/{self.codeOReBUT}")
        donnees = ET.SubElement(donnees_xml, "ressource")
        donnees.set("code", self.codeOReBUT)
        donnees.set("ordre", str(self.ordre)) # ordre dans le semestre
        donnees.set("heuresCMTD", str(self.heures["pn"]["encadrees"]))
        donnees.set("heuresTP", str(self.heures["pn"]["tp"]))
        if self.heures["acd"]["cm"]:
            donnees.set("heuresCM", str(self.heures["acd"]["cm"]))
        if self.heures["acd"]["td"]:
            donnees.set("heuresTD", str(self.heures["acd"]["td"]))
        # Titre
        titre = ET.SubElement(donnees, "titre")
        titre.text = self.nom
        # Description
        description = ET.SubElement(donnees, "description")
        descr = self.to_xml_prepare_description()
        descr = rpn.latex.remplace_id_par_code_orebut(descr)
        description.text = "\n" + descr + "\n"
        # Mots-clés
        motscles = ET.SubElement(donnees, "mots-cles")
        motscles.text = self.yaml["motscles"]
        # ACs
        self.to_xml_ajoute_acs(donnees)
        # Compétences
        self.to_xml_ajoute_competences(donnees)
        # Les pré-requis
        prerequis = self.yaml["prerequis"]

        if prerequis == "Aucun":
            prerequis = []
        self.to_xml_ajoute_liste_activites(donnees, prerequis, chapeau="prerequis",
                                           champ="ressource")
        # Les SAes
        saes = self.yaml["sae"]
        self.to_xml_ajoute_liste_activites(donnees, saes, chapeau="saes", champ="sae")
        # Parcours
        self.to_xml_ajoute_parcours(donnees)
        # coeffs

        return

    def to_bdd(self, connexion, numero_semestre):
        """Ajoute la ressource à la BDD pointée par la BDD"""

        # Ajout en tant qu'activité
        contexte = self.yaml["contexte"]
        if contexte:
            contexte = rpn.latex.remplace_id_par_code_orebut(contexte)
        else:
            contexte = ""
        description = self.yaml["contenus"]
        if description:
            description = rpn.latex.remplace_id_par_code_orebut(description)
        else:
            description = ""

        id_activite = self.to_bdd_activite(connexion, numero_semestre, self.codeOReBUT,
                                           self.nom, self.ordre, "", contexte, description)

        # Ajoute les éléments spécifiques à la ressource
