"""
Module dédié aux éléments synthétiques (extrait des tableaux de synthèse
maintenu par le PACD) sur les ressources, les SAés, les Compétences et les ACS.
Parmi ces éléments, les codes et les noms officiels ; des tags pour calcul
d'information de répartition.

Les ressources sont décrites dans un fichier yaml/ressources.yml structuré par semestre
et par code, de la forme :

.. code-block:: python

   S1:
     R101:
       nom: "Initiation aux réseaux informatiques"
       tags-thematiques: ["réseau", "métier"]

Idem pour les SAés, dans yaml/saes.yml de la forme :

.. code-block:: python

   S1:
     SAE11:
       nom: "..."
       tags-thematiques: ["métier"]

"""

import logging, yaml
import tools
import rofficiel.competences
from tools import supprime_accent_espace

__LOGGER = logging.getLogger(__name__)

# *************************************** #
# Récupère les informations officielles  #
## Les ressources
def get_DATA_RESSOURCES(repertoire = "yaml/pn"):
    """Récupère les informations sur les ressources (triées par semestre
    et par nom), en extrayant les données du fichier yaml/ressources.yml.
    """
    with open(repertoire + "/ressources.yml", 'r', encoding="utf8") as fid:
        DATA_RESSOURCES = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_RESSOURCES

## Les intitules des compétences
def get_DATA_COMPETENCES(repertoire = "yaml/pn"):
    """Récupère les informations des ACS,
    en extrayant les données du fichier yaml/competences.yml"""
    with open(repertoire +"/competences.yml", 'r', encoding="utf8") as fid:
        DATA_COMPETENCES = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_COMPETENCES


## Les ACS
def get_DATA_ACS(repertoire = "yaml/pn"):
    """Récupère les informations des ACS,
    en extrayant les données du fichier yaml/competences.yml"""
    with open(repertoire +"/acs.yml", 'r', encoding="utf8") as fid:
        DATA_ACS = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_ACS

## Les SAEs
def get_DATA_SAES(repertoire = "yaml/pn"):
    """Récupère les informations des SAés (triées par semestre et par nom),
    en extrayant les données du fichier yaml/saes.yml"""
    with open(repertoire +"/saes.yml", 'r', encoding="utf8") as fid:
        DATA_SAES = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_SAES

## Les abréviations
def get_DATA_ABBREVIATIONS(repertoire = "yaml"):
    with open(repertoire +"/abbreviations.yml", "r", encoding="utf8") as fid:
        DATA_ABBREVIATIONS = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_ABBREVIATIONS

## Les mostcles (commandes, logiciels, etc...)
def get_MOTS_CLES(repertoire = "yaml"):
    with open(repertoire +"/motscles.yml", "r", encoding="utf8") as fid:
        DATA_MOTSCLES = yaml.load(fid.read(), Loader=yaml.Loader)
    return DATA_MOTSCLES

## L'association code ressource -> fichier google
def get_DATA_R_DOCX(repertoire = "yaml/pn"):
    # récupère les ressources
    DATA_R_DOCX = {}
    with open(repertoire +"/ressources.yml", "r", encoding="utf8") as fid:
        donnees_ressources = yaml.load(fid.read(), Loader=yaml.Loader)
    with open(repertoire +"/saes.yml", "r", encoding="utf8") as fid:
        donnees_saes = yaml.load(fid.read(), Loader=yaml.Loader)
    for sem in donnees_ressources:
        if sem not in DATA_R_DOCX:
            DATA_R_DOCX[sem] = {}
        for parcours in donnees_ressources[sem]:
            DATA_R_DOCX[sem][parcours] = {}
            for code in donnees_ressources[sem][parcours]:
                if code in DATA_R_DOCX:
                    __LOGGER.error(f"get_DATA_R_DOCX: {code} déjà présent dans DATA_R_DOCX du sem")
                DATA_R_DOCX[sem][parcours][code] = donnees_ressources[sem][parcours][code]["docx"]

        for parcours in donnees_saes[sem]:
            if parcours not in DATA_R_DOCX[sem]:
                DATA_R_DOCX[sem][parcours] = {}
            for code in donnees_saes[sem][parcours]:
                if code in DATA_R_DOCX:
                    __LOGGER.error(f"get_DATA_R_DOCX: {code} déjà présent dans DATA_R_DOCX du sem")
                DATA_R_DOCX[sem][parcours][code] = donnees_saes[sem][parcours][code]["docx"]
    return DATA_R_DOCX

AUCUN_PREREQUIS = "Aucun"

PARCOURS = ["Cyber", "DevCloud", "IOM", "PilPro", "ROM"]

PHRASE_ANGLAIS = "L'étudiant pourra être amené à exploiter des documents vidéos et/ou écrits en anglais (et/ou dans une autre langue étrangère), et en présenter une synthèse à l'écrit et/ou à l'oral, en anglais (et/ou dans une autre langue étrangère.)"

class Officiel():
    """
    Récupère et stocke des informations dites *officielles*, fournies par différents
    fichiers yaml (conçus manuellement).

    Certaines traduisent les tableurs de synthèse du GT-BUT avec :

    * le nom des ressources (cf. ressources.yml)
    * le nom des saés (cf. saes.yml)
    * les compétences (cf. RT123_a_supprimer.yml)
    * les  apprentissages critiques (structurées par compétences) (cf. acs.yml)

    D'autres donnent : les mots-clés, les abréviations propres à RT (cf. motscles.yml, abreviations.yml).

    Fournit également différentes méthodes pour exploiter ces données officielles.
    """

    def __init__(self):

        ## Les ressources
        self.DATA_RESSOURCES = get_DATA_RESSOURCES()
        ## Les ACS
        self.DATA_ACS = get_DATA_ACS()
        ## Les SAEs
        self.DATA_SAES = get_DATA_SAES()
        ## Les compétences
        self.DATA_COMPETENCES = get_DATA_COMPETENCES()
        self.DATA_COMPETENCES_DETAILLEES = rofficiel.competences.Competence.get_DATA_COMPETENCES_DETAILLEES()
        self.competences = {}
        for code_comp in self.DATA_COMPETENCES_DETAILLEES:
            self.competences[code_comp] = rofficiel.competences.Competence(code_comp, self.DATA_COMPETENCES_DETAILLEES[code_comp])
        ## Les abréviations
        self.DATA_ABBREVIATIONS = get_DATA_ABBREVIATIONS()
        ## Les mostcles (commandes, logiciels, etc...)
        self.DATA_MOTSCLES = get_MOTS_CLES()
        ## L'association ressources -> fichier.rdocx
        self.DATA_R_DOCX = get_DATA_R_DOCX()


    def get_ressource_name_by_code(self, code):
        """Pour un code de ressource valide (existant dans ``DATA_RESSOURCES``)
        et ce, sans connaissance de l'année, ni du semestre, fournit le nom officiel de la ressource
        """
        return get_officiel_name_by_code_using_dict(code, self.DATA_RESSOURCES)

    def get_code_orebut_from_code(self, code):
        """Renvoie un code orebut d'après un code RX.XX ou SAEX.XX"""
        return get_code_orebut_from_id(code)

    def get_noms_niveaux(self):
        """Partant de la description détaillée des compétences DATA_COMPETENCES_DETAILLES, renvoie le nom
        des niveaux triés par année sous la forme d'un dictionnaire :
        { 'BUT1': {'RT1': [niveau1, niveau2, niveau3]}, ... } """
        niveaux = {"BUT{}".format(i): {} for i in range(1, 4)}
        for annee in niveaux:
            comp_communes = {"RT{}".format(i): [] for i in range(1, 4)}
            if annee == "BUT1": # ajout des compétences de parcours
                niveaux[annee] = comp_communes
            else:
                comp_parcours = {"{}{}".format(p, i): None for i in range(1, 3) for p in PARCOURS}
                niveaux[annee] = {**comp_communes, **comp_parcours}

        for comp in self.DATA_COMPETENCES_DETAILLEES:
            nbre_niveaux = len(self.DATA_COMPETENCES_DETAILLEES[comp]["niveaux"])
            nivs = list(self.DATA_COMPETENCES_DETAILLEES[comp]["niveaux"].keys())
            if nbre_niveaux == 3:
                assert "RT" in comp, "officiel: Pb dans les niveaux de la comp"
                for annee in range(0, 3): # l'année
                    assert comp in niveaux["BUT{}".format(annee+1)], f"officiel: {comp} manquante"
                    niveaux["BUT{}".format(annee+1)][comp] = nivs[annee]
            else:
                for annee in range(0, 2): # l'année
                    assert comp in niveaux["BUT{}".format(annee+2)], f"officiel: {comp} manquante"
                    niveaux["BUT{}".format(annee+2)][comp] = nivs[annee]
        return niveaux


    def get_sae_name_by_code(self, code):
        """Pour un code de saé (en notation pointé, et existant dans ``DATA_SAES``)
        et ce sans connaissance du semestre,  fournit le nom officiel de la sae
        """
        return get_officiel_name_by_code_using_dict(code, self.DATA_SAES)

    def get_sem_activite_by_code(self, code):
        """Récupère le semestre d'une activité (ressource ou SAé) d'après son ``code``
        """
        for sem in self.DATA_RESSOURCES:
            for p in self.DATA_RESSOURCES[sem]:
                if code in self.DATA_RESSOURCES[sem][p]:
                    return sem

        for sem in self.DATA_SAES:
            for p in self.DATA_SAES[sem]:
                if code in self.DATA_SAES[sem][p]:
                    return sem
        # sinon non trouvé


    def get_docx_file_by_code(self, code):
        """Renvoie le nom du fichier rdocx contenant le descriptif d'une ressource ou d'une SAE
        à l'aide de son code"""
        for sem in self.DATA_R_DOCX:
            for p in self.DATA_R_DOCX[sem]:
                if code in self.DATA_R_DOCX[sem][p]:
                    return self.DATA_R_DOCX[sem][p][code]
        return None


    def get_comp_from_acs_code(self, code_acs):
        """Renvoie le nom/code de la comp en utilisant le code (notation pointé) d'un acs"""
        for annee in self.DATA_ACS:
            for comp in self.DATA_ACS[annee]:
                if code_acs in self.DATA_ACS[annee][comp]:
                    return comp
        return None

    def get_annee_from_acs(self, code_acs):
        """Renvoie l'année correspondant à un acs"""
        for annee in self.DATA_ACS:
            for comp in self.DATA_ACS[annee]:
                if code_acs in self.DATA_ACS[annee][comp]:
                    return annee
        return None

    @staticmethod
    def get_annee_from_semestre(sem):
        """Renvoie l'année en fonction d'un semestre"""
        if isinstance(sem, str):
            if sem in ["1", "2"]:
                return "BUT1"
            elif sem in ["3", "4"]:
                return "BUT2"
            elif sem in ["5", "6"]:
                return "BUT3"
            else:
                return "BUTC"
        else: # si liste
            annees = []
            for s in sem:
                annees.append(Officiel.get_annee_from_semestre(s))
            return sorted(list(set(annees)))

    def get_diminutif_comp_from_code_comp(self, code_comp):
        """Renvoie le diminutif (nom court) d'une compétence en fonction de son code_comp (par ex. RT1, RT2, ...)"""
        if code_comp in self.DATA_COMPETENCES_DETAILLEES:
            return self.DATA_COMPETENCES_DETAILLEES[code_comp]["diminutif"]

    def to_bdd(self, connexion):
        """Insère les données du pn dans la bdd pointée par connexion"""
        # Création des compétences
        for comp in self.competences:
            self.competences[comp].to_bdd(connexion)


def devine_code_activite_by_nom_from_dict(champ, dico):
    """Partant d'une chaine de caractères décrivant une ressource, une SAé ou un ACS,
    détermine le code présent dans le dico officiel (dico à double entrée),
    Le dico officiel vient d'un .yml"""
    codes = []
    champ_purge = tools.supprime_accent_espace(champ)
    for comp in dico:
        for code in dico[comp]:
            code_purge = tools.supprime_accent_espace(dico[comp][code])
            if code_purge in champ_purge:
                codes += [code]
    return sorted(list(set(codes)))



def get_officiel_name_by_code_using_dict(code, dico):
    """Extrait un nom à partir d'un code (pour les RESSOURCES ou les SAES)"""
    for sem in dico:
        for p in dico[sem]: # parcours
            for rcode in dico[sem][p]:
                if rcode==code:
                    return dico[sem][p][code]["intitule"]

def get_code_from_nom_using_dict(nom, dico):
    """Récupère le code d'une ressource d'après son nom en utilisant les noms officiels
    des ressources du yaml si dico == DATA_RESSOURCES ; sinon fait de même avec les SAE"""
    nom = tools.supprime_accent_espace(nom)
    for sem in dico:
        for p in dico[sem]:
            for code in dico[sem][p]:
                nom_data = tools.supprime_accent_espace(dico[sem][p][code]["intitule"])
                if nom.startswith(nom_data):
                    return code

def get_code_orebut_from_id(code):
    """Renvoie un code orebut d'après un code/id RX.XX ou SAEX.XX"""
    DATA_RESSOURCES = get_DATA_RESSOURCES()
    DATA_SAES = get_DATA_SAES()
    for sem in DATA_RESSOURCES:
        for p in DATA_RESSOURCES[sem]:
            for rcode in DATA_RESSOURCES[sem][p]:
                if rcode == code:
                    return DATA_RESSOURCES[sem][p][code]["orebut"]

    for sem in DATA_SAES:
        for p in DATA_SAES[sem]:
            for scode in DATA_SAES[sem][p]:
                if scode == code:
                    return DATA_SAES[sem][p][code]["orebut"]

if __name__=="__main__":
    import pprint
    pn = Officiel()
    niveaux = pn.get_noms_niveaux()
    pprint.pprint(niveaux)


def devine_code_comp_by_nom_from_dict(champ):
    """Partant d'une chaine de caractères décrivant une compétence,
    détermine le code présent dans le dico officiel,
    Le dico officiel vient d'un .yml"""
    comp = []
    DATA_COMPETENCES = get_DATA_COMPETENCES()
    champ_purge = supprime_accent_espace(champ)
    for code in DATA_COMPETENCES:
        comp_purge = supprime_accent_espace(DATA_COMPETENCES[code])
        if champ_purge in comp_purge:
            comp += [code]
    return sorted(list(set(comp)))