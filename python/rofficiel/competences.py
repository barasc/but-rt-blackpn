import logging

import yaml
import rbdd.rbdd, sqlite3

from tools import supprime_accent_espace


class Competence():
    """ Modélise une compétence """

    __LOGGER = logging.getLogger(__name__)

    def __init__(self, code, data_yaml):
        """Initialise une compétence à partir des données extraites d'un yaml"""
        self.code = code # le code Cyber1/RT1
        self.nom = data_yaml["nom"]
        self.diminutif = data_yaml["diminutif"]
        self.description = data_yaml["description"]
        self.yaml = data_yaml
        self.statut = "TC" if len(data_yaml["niveaux"]) == 3 else "parcours"
        self.niveaux = []
        for (i, niv) in enumerate(data_yaml["niveaux"]):
            if self.statut == "TC":
                val_niveau = i+1
            else:
                val_niveau = i+2
            niveau = Niveau(val_niveau, self.statut, niv, data_yaml["niveaux"][niv], self.code)
            self.niveaux.append(niveau)
        # Les acs tous niveaux confondus
        self.acs = {}
        for niv in self.niveaux:
            self.acs = {**self.acs, **niv.acs}

        # Les composantes essentielles
        self.composantes = data_yaml["composantes"]
        # Les situations prof
        self.situations = data_yaml["situations"]

    def to_bdd(self, connexion):
        """Création de la comp dans la bdd pointée par connexion"""
        requete = f"""INSERT INTO competences (code, nom, diminutif, description)
                     VALUES ("{self.code}", "{self.nom}", "{self.diminutif}", "{self.description}")"""

        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            nbre_ligne_ajoute = curseur.rowcount
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.warning(f"competence.to_bdd: Compétence {self.code} déjà inserée")
        except sqlite3.Error as e:
            nbre_ligne_ajoute = 0
            self.__LOGGER.warning("competence.to_bdd: Insertion de la compétence non réalisée")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")

        # Ajout des niveaux
        for niveau in self.niveaux:
            niveau.to_bdd(connexion)

        # Les composantes essentielles
        for compo in self.composantes:
            self.to_bdd_composante(connexion, compo, self.composantes[compo])

        # Les situations pro
        for situ in self.situations:
            self.to_bdd_situation(connexion, situ)

    def to_bdd_composante(self, connexion, code_compo, intitule):
        """Insère une composante essentielle"""
        requete = f"""INSERT INTO composantes (code, intitule, code_comp)
                             VALUES ("{code_compo}", "{intitule}", "{self.code}")"""

        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.info("competence.to_bdd_compo: Composante déjà insérée")
        except sqlite3.Error as e:
            self.__LOGGER.warning("competence.to_bdd: Insertion de la compétence non réalisée")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")

    def to_bdd_situation(self, connexion, situ):
        """Insère une composante essentielle"""
        requete = f"""INSERT INTO situations (intitule, code_comp)
                             VALUES ("{situ}", "{self.code}")"""

        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.info("competence.to_bdd_situtation: Composante déjà insérée")
        except sqlite3.Error as e:
            self.__LOGGER.warning("competence.to_bdd_situtation: Insertion de la situation dans la compétence non réalisée")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")

    @staticmethod
    def get_DATA_COMPETENCES_DETAILLEES(repertoire = "yaml/competences"):
        DATA_COMPETENCES = {}
        fichier = ["RT1.yml", "RT2.yml", "RT3.yml", "IOM.yml", "Cyber.yml", "ROM.yml", "DevCloud.yml", "PilPro.yml"]
        for f in fichier:
            chemin = repertoire + "/" + f
            with open(chemin, 'r', encoding="utf8") as fid:
                donnees = yaml.load(fid.read(), Loader=yaml.Loader)
                DATA_COMPETENCES = {**DATA_COMPETENCES, **donnees} # fusion dictionnaire
        return DATA_COMPETENCES


class Niveau():
    """Modélise un niveau"""
    __LOGGER = logging.getLogger(__name__)

    def __init__(self, niveau, type, intitule, acs, code_comp):
        self.niveau = niveau
        self.code_comp = code_comp # la comp à laquelle appartient le niveau
        self.type = type # 'TC' ou 'parcours'
        self.intitule = intitule
        self.acs = {} # le dictionnaire des AC
        for ac in acs:
            self.acs[ac] = AC(ac, acs[ac], self.code_comp, self.niveau)

    def to_bdd(self, connexion):
        """Ajout le niveau dans la BDD"""
        requete = f"""INSERT INTO niveaux (niveau, intitule, code_comp) 
                      VALUES ({self.niveau}, "{self.intitule}", "{self.code_comp}");"""
        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            id_niveau = curseur.lastrowid # l'id du dernier inséré
            connexion.commit()
        except sqlite3.IntegrityError as e:
            id_niveau = self.get_id_niveau_from_bdd(connexion)
        except sqlite3.Error as e:
            id_niveau = None
            self.__LOGGER.error("niveau.to_bdd: Insertion du niveau non réalisé")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")

        if id_niveau:
            for ac in self.acs:
                self.acs[ac].to_bdd(connexion, id_niveau)


    def get_id_niveau_from_bdd(self, connexion):
        """Récupère l'id_niveau d'un niveau de comp dans la BDD"""
        requete = f"""SELECT id FROM niveaux WHERE niveau={self.niveau} and 
                        code_comp="{self.code_comp}"
                    """
        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            id_niveau = curseur.fetchone()[0]
            connexion.commit()
        except:
            self.__LOGGER.warning("Pas de récup du niveau :(")
            id_niveau = None
        return id_niveau


    def get_acs(self):
        return self.acs

class AC():
    """ Modélise un ac  """

    __LOGGER = logging.getLogger(__name__)

    def __init__(self, code, intitule, code_comp, niveau):
        self.code = code
        self.intitule = intitule
        self.code_comp = code_comp # le code de la comp incluant l'acs
        self.niveau = niveau # le niveau correspondant à la comp

    def to_bdd(self, connexion, id_niveau):
        """Insère l'ac dans la bdd pointée par connexion"""
        requete = f"""INSERT INTO acs (code, intitule, id_niveau) 
                              VALUES ("{self.code}", "{self.intitule}", {id_niveau});"""
        try:
            curseur = connexion.cursor()
            curseur.execute(requete)
            nbre_ligne_ajoute = curseur.rowcount
            connexion.commit()
        except sqlite3.IntegrityError as e:
            self.__LOGGER.error(f"ac.to_bdd: AC {self.code} déjà inséré")
        except sqlite3.Error as e:
            nbre_ligne_ajoute = 0
            self.__LOGGER.error("ac.to_bdd: Insertion de l'ac non réalisé")
            self.__LOGGER.warning(f"Unexpected {e}, {type(e)}")

    @staticmethod
    def get_acs_notation_pointe(code_acs):
        """Partant d'un code d'acs de la forme ACXXXX, renvoie
        la notation pointée ACXX.XX"""
        return code_acs[:4] + "." + code_acs[4:]


def devine_code_acs_by_nom_from_dict(champ, dico):
    """Partant d'une chaine de caractères décrivant une ressource, une SAé ou un ACS,
    détermine le code présent dans le dico officiel (dico à double entrée),
    Le dico officiel vient d'un .yml"""
    acs = []
    champ_purge = supprime_accent_espace(champ)
    for annee in dico:
        for comp in dico[annee]:
            for code in dico[annee][comp]:
                acs_purge = supprime_accent_espace(dico[annee][comp][code])
                if acs_purge in champ_purge:
                    acs += [code]
    return sorted(list(set(acs)))


